package com.water.drinkingwater.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.water.drinkingwater.HomeActivity;
import com.water.drinkingwater.R;
import com.water.drinkingwater.baseview.BaseActivity;

import company.librate.RateDialog;

public class Fragment_settings extends Fragment implements View.OnClickListener , BaseActivity.getTimePicker{
    BaseActivity context;
    LinearLayout btn_remider_ngu, btn_home, btn_statistical, btn_policy, btn_rate_app, btn_feedback;
    HomeActivity homeActivity;
    RateDialog rateDialog;
    public Fragment_settings(BaseActivity context) {
        this.context = context;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_more, container, false);
        context.creatModelTime(this);
        rateDialog = new RateDialog(context, "", true);
        btn_remider_ngu = view.findViewById(R.id.btn_remider_ngu);
        btn_home = view.findViewById(R.id.btn_home);
        btn_statistical = view.findViewById(R.id.btn_statistical);
        btn_policy = view.findViewById(R.id.btn_policy);
        btn_rate_app = view.findViewById(R.id.btn_rate_app);
        btn_feedback = view.findViewById(R.id.btn_rate_app);


        btn_remider_ngu.setOnClickListener(this);
        btn_home.setOnClickListener(this);
        btn_statistical.setOnClickListener(this);
        btn_policy.setOnClickListener(this);
        btn_rate_app.setOnClickListener(this);
        btn_feedback.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_remider_ngu:
                context.showTimePickerDialog();
                break;
            case R.id.btn_home:
                homeActivity.addWater();
                break;
            case R.id.btn_statistical:
                homeActivity.addFChart();
                break;
            case R.id.btn_policy:
                Uri uri = Uri.parse(getString(R.string.policy));
                Intent intent1 = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent1);

                break;
            case R.id.btn_rate_app:
                rateDialog.show();

                break;
            case R.id.btn_feedback:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse(getString(R.string.mail)));
                startActivity(Intent.createChooser(intent, "Send feedback"));
                break;


        }

    }

    @Override
    public void gettimePicker(String timepicker,int hour) {
        context.setmHourBed(hour);

    }
}
