package com.water.drinkingwater.fragment;

import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.water.drinkingwater.MyApplication;
import com.water.drinkingwater.R;
import com.water.drinkingwater.adapter.Adapter_Water_Chart;
import com.water.drinkingwater.baseview.BaseActivity;
import com.water.drinkingwater.baseview.DialogAddWeight;
import com.water.drinkingwater.chartView.DayAxisValueFormatter;
import com.water.drinkingwater.chartView.MyValueFormatter;
import com.water.drinkingwater.chartView.XYMarkerView;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.EntityWeight;
import com.water.drinkingwater.object.Entity_Water_Day;
import com.water.drinkingwater.object.Entity_water;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MastChartWaterFragment extends Fragment implements OnChartValueSelectedListener, AdapterView.OnItemSelectedListener {
    private BarChart chart;
    //    private SeekBar seekBarX, seekBarY;
    private TextView tvX, tvY;
    BaseActivity baseActivity;
    ArrayList<EntityWeight> arrayList_Weight = new ArrayList<>();
    @BindView(R.id.sp_select_yeah)
    Spinner sp_select_yeah;
    @BindView(R.id.btn_add_weight)
    LinearLayout btnadd_weight;
    View view;
    @BindView(R.id.recyclerview_water)
    RecyclerView recyclerview_water;
    //    @BindView(R.id.mConstraintLayout)
//    ConstraintLayout mConstraintLayout;
    List<String> categories = new ArrayList<String>();

    public MastChartWaterFragment(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    private ArrayList<Entity_water> arrayList_water = new ArrayList<>();
    private ArrayList<Entity_Water_Day> arrayList_water_day = new ArrayList<>();

    DBManager dbManager;
    DialogAddWeight.UpdateChartWeight updateChartWeight = new DialogAddWeight.UpdateChartWeight() {
        @Override
        public void updateChartWeight() {
            intView(view);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chartbar, container, false);
        dbManager = new DBManager(baseActivity);

        intView(view);
        ButterKnife.bind(this, view);

        onclickView();

        categories.add(baseActivity.getYear() + "");
        categories.add(baseActivity.getYear() - 1 + "");
        categories.add(baseActivity.getYear() - 2 + "");
        categories.add(baseActivity.getYear() - 3 + "");
        categories.add(baseActivity.getYear() - 4 + "");
        categories.add(baseActivity.getYear() - 5 + "");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sp_select_yeah.setAdapter(dataAdapter);
        chart.animateXY(2000, 2000);

        //====================================== CHART OVAL ========================================
        GridLayoutManager manager = new GridLayoutManager(baseActivity, 3, GridLayoutManager.VERTICAL, false);

        recyclerview_water.setLayoutManager(manager);
        arrayList_water.addAll(dbManager.gettWaterWhere());
        arrayList_water_day.addAll(dbManager.getWaterDayNoW());
        Adapter_Water_Chart adapter_water_chart = new Adapter_Water_Chart(arrayList_water, arrayList_water_day, baseActivity, getContext());
        recyclerview_water.setAdapter(adapter_water_chart);
        adapter_water_chart.notifyDataSetChanged();

        return view;
    }






    private void onclickView() {
        btnadd_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogAddWeight.Builder(baseActivity).setUpdateChartWeight(updateChartWeight).build();
            }
        });
    }

    public void intView(View view) {
        arrayList_Weight.clear();
        arrayList_Weight.addAll(dbManager.getWeight(baseActivity.getYear() + ""));
        chart = view.findViewById(R.id.chart1);
        chart.setOnChartValueSelectedListener(this);

        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);

        chart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(300);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawGridBackground(false);
        // chart.setDrawYLabels(false);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(tfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day chỉ khoảng thời gian 1 ngày
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        for (int i = 0; i < arrayList_Weight.size(); i++) {
            xAxis.setValueStringmastter(arrayList_Weight.get(i).getDate_month());
        }

        ValueFormatter custom = new MyValueFormatter("Kg");

        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(tfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
//        rightAxis.setTypeface(tfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(MyApplication.getAppContext(), xAxisFormatter);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        chart.setSelected(false);
        chart.setAutoScaleMinMaxEnabled(false);
        chart.setMaxHighlightDistance(50f);



        // setting data
//        seekBarY.setProgress(50);
//        seekBarX.setProgress(12);

        // chart.setDrawLegend(false);

        setData(7, 70.0f);
    }

    private final RectF onValueSelectedRectF = new RectF();

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;

        RectF bounds = onValueSelectedRectF;
        chart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = chart.getPosition(e, YAxis.AxisDependency.LEFT);

        Log.i("bounds", bounds.toString());
        Log.i("position", position.toString());

        Log.i("x-index",
                "low: " + chart.getLowestVisibleX() + ", high: "
                        + chart.getHighestVisibleX());


        MPPointF.recycleInstance(position);


        int p = (int) e.getX();

//        new UIDialogImageCustom.Builder(baseActivity).setTitle("Detail").setMessage("Weight: " + arrayList_Weight.get(p).getWeight() + baseActivity.getmValueW() + "\n\n" + "Date: " + arrayList_Weight.get(p).getDate_month() + "-" + arrayList_Weight.get(p).getYeah())
//                .setContentButtonSubmit("Ok").setBackgroudDrawableButtonSubmis(R.drawable.custom_ripple_border_active).setTextColorSubmis(Color.WHITE).
//                setButtonSubmisListerner(new UIDialogImageCustom.UIDialogCustomListener() {
//                    @Override
//                    public void onClick(Dialog dialog) {
//                        dialog.dismiss();
//                    }
//                }).build();


//        Toast.makeText(baseActivity, arrayList_Weight.get(p).getDate_month(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }

    private void setData(int count, float range) {

        float start = 1f;

        ArrayList<BarEntry> values = new ArrayList<>();


        for (int i = 0; i < arrayList_Weight.size(); i++) {

            float val = arrayList_Weight.get(i).getWeight();
            values.add(new BarEntry(i, val));
//            if (val * 100 < 25) {
//                values.add(new BarEntry(i, val, getResources().getDrawable(R.drawable.star)));
//            } else {
//
//            }


        }


//
//        for (int i = (int) start; i < start + count; i++) {
//            float val = (float) (Math.random() * (range + 1));
//
//            if (Math.random() * 100 < 25) {
//                values.add(new BarEntry(i, val, getResources().getDrawable(R.drawable.star)));
//            } else {
//                values.add(new BarEntry(i, val));
//            }
//        }

        BarDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            int startColor1 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_orange_light);
            set1.setBarShadowColor(startColor1);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, "Weight Parameters");

            set1.setDrawIcons(false);

//            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            /*int startColor = ContextCompat.getColor(this, android.R.color.holo_blue_dark);
            int endColor = ContextCompat.getColor(this, android.R.color.holo_blue_bright);
            set1.setGradientColor(startColor, endColor);*/

            int startColor1 = ContextCompat.getColor(MyApplication.getAppContext(), R.color.color_chart);
            int startColor2 = ContextCompat.getColor(MyApplication.getAppContext(), R.color.defau_color_btn);
//            int startColor3 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_orange_light);
//            int startColor4 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_green_light);
//            int startColor5 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_red_light);
//            int endColor1 = ContextCompat.getColor( MyApplication.getAppContext(), android.R.color.holo_blue_dark);
//            int endColor2 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_purple);
//            int endColor3 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_green_dark);
//            int endColor4 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_red_dark);
//            int endColor5 = ContextCompat.getColor(MyApplication.getAppContext(), android.R.color.holo_orange_dark);

            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(startColor2, startColor1));
//            gradientColors.add(new GradientColor(startColor2, endColor2));
//            gradientColors.add(new GradientColor(startColor3, endColor3));
//            gradientColors.add(new GradientColor(startColor4, endColor4));
//            gradientColors.add(new GradientColor(startColor5, endColor5));

            set1.setGradientColors(gradientColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
//            data.setValueTypeface(tfLight);
            data.setBarWidth(0.9f);

            chart.setData(data);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
