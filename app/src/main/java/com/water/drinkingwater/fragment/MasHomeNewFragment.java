package com.water.drinkingwater.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.water.drinkingwater.HomeActivity;
import com.water.drinkingwater.MyApplication;
import com.water.drinkingwater.R;
import com.water.drinkingwater.adapter.Adapter_Water;
import com.water.drinkingwater.adapter.Adapter_sizeWater;
import com.water.drinkingwater.baseview.BaseActivity;
import com.water.drinkingwater.baseview.DialogCustomWater;
import com.water.drinkingwater.baseview.DialogEditTotalWater;
import com.water.drinkingwater.baseview.DialogEditWater;
import com.water.drinkingwater.baseview.mTextview;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.EntityCustomSizeWater;
import com.water.drinkingwater.object.Entity_Water_Day;
import com.water.drinkingwater.object.Entity_water;
import com.water.drinkingwater.object.Item_kg;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class MasHomeNewFragment extends Fragment implements View.OnClickListener, DialogCustomWater.sendEvent, Adapter_sizeWater.longClickItemWatet,
        Adapter_sizeWater.OnclickWater, Adapter_Water.OnclickWaterSale, DialogEditWater.copyWater, BaseActivity.getdatePicker, DialogEditTotalWater.OnclickUpdateWater {
    private ImageView btn_add_option;
    private RelativeLayout ll_custom_size;
    private View closeView;
    private HomeActivity activity;
    private ArrayList<String> strings = new ArrayList<>();
    private LinearLayout btn_custom_size;
    private DBManager dbManager;
    private RecyclerView recyclerview_size_water;
    private ArrayList<EntityCustomSizeWater> arrayListSizeWater = new ArrayList<>();
    private ArrayList<Entity_water> arrayList_water = new ArrayList<>();
    private ArrayList<Entity_Water_Day> arrayList_water_day = new ArrayList<>();
    private Adapter_sizeWater adapter_water;
    private Adapter_Water adapter_newwater;
    @BindView(R.id.recyclerview_water)
    RecyclerView recyclerview_water;
    @BindView(R.id.select_day)
    mTextview select_day;
    @BindView(R.id.btn_ago_day)
    AppCompatImageView btn_ago_day;
    @BindView(R.id.btn_nextday)
    AppCompatImageView btn_nextday;
    @BindView(R.id.water_total)
    mTextview water_total;
    @BindView(R.id.water_value)
    mTextview water_value;
    private String date;
    int keyago = 0;
    @BindView(R.id.txt_water_day)
    mTextview txt_water_day;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btn_total_water)
    RelativeLayout btn_total_water;
    String unit_value;
    @BindView(R.id.none_data)
    mTextview none_data;
    @BindView(R.id.switch_remider)
    ImageView switch_remider;
    boolean aBoolean = false;

    public MasHomeNewFragment(HomeActivity activity) {
        this.activity = activity;
    }


    @OnClick(R.id.btn_add_option)
    public void onclick_add_option() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mas_fragment_home_new, container, false);
        ButterKnife.bind(this, view);
        dbManager = new DBManager(MyApplication.getAppContext());
        intView(view);
        strings = activity.loadacessSticker("water");
        Log.e("duongnk", "onCreateView: " + activity.getDate() + "//" + activity.getTime());
        return view;
    }

    private void intView(View view) {
        activity.creatModel(this);
        unit_value = " " + activity.getmValueW();
        Log.e("duongnk", "intView: " + activity.getDay());
        date = activity.getDate();
        activity.setbackupday(date);
        arrayList_water.addAll(dbManager.gettWaterWhereDate(date));
        btn_nextday.setVisibility(View.GONE);
        recyclerview_water.setLayoutManager(new GridLayoutManager(MyApplication.getAppContext(), 4));
        adapter_newwater = new Adapter_Water(arrayList_water, MyApplication.getAppContext(), this);
        recyclerview_water.setAdapter(adapter_newwater);
        arrayListSizeWater = dbManager.getSizeWaterDB();
        ll_custom_size = view.findViewById(R.id.ll_custom_size);
        btn_add_option = view.findViewById(R.id.btn_add_option);
        closeView = view.findViewById(R.id.closeView);
        water_total.setText(activity.getWaterofday() + "");
        btn_ago_day.setOnClickListener(this);
        btn_nextday.setOnClickListener(this);
        select_day.setOnClickListener(this);
        switch_remider.setOnClickListener(this);
        btn_total_water.setOnClickListener(this);
        recyclerview_size_water = view.findViewById(R.id.recyclerview_size_water);
        recyclerview_size_water.setLayoutManager(new LinearLayoutManager(MyApplication.getAppContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter_water = new Adapter_sizeWater(arrayListSizeWater, MyApplication.getAppContext(), activity.pathfileIconWaterAssets, this::longClickItem, this::onclickwater);
        recyclerview_size_water.setAdapter(adapter_water);
        closeView.setOnClickListener(this);
        btn_custom_size = view.findViewById(R.id.btn_custom_size);
        btn_custom_size.setOnClickListener(this);
        txt_water_day.setText(activity.getWaterday(date) + "");
        setUpProget(date);
        view.findViewById(R.id.btn_add_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_custom_size.setVisibility(View.VISIBLE);
                btn_add_option.setVisibility(View.GONE);
                closeView.setVisibility(View.VISIBLE);
            }
        });

    }

    private void setUpProget(String date) {
        arrayList_water_day.clear();
        arrayList_water_day.addAll(dbManager.getWaterDay(date));
        if (arrayList_water_day.size() > 0) {
            // set max lượng nước phải uống trong 1 ngày
            progressBar.setMax(arrayList_water_day.get(arrayList_water_day.size() - 1).getWater());
            // set lượng nước đã uống trong ngày
            progressBar.setProgress(activity.getWaterday(date));

            water_total.setText(arrayList_water_day.get(arrayList_water_day.size() - 1).getWater() + "");
            water_value.setText(unit_value);

        } else {
//            Toast.makeText(activity, "No data", Toast.LENGTH_SHORT).show();
        }
        if (progressBar.getProgress() == progressBar.getMax()) {

            Dialog dialog = new Dialog(activity);
            dialog.setContentView(R.layout.dialog_done_water);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mTextview date_dialgo = dialog.findViewById(R.id.txt_date);
            Button btn_done = dialog.findViewById(R.id.btn_done);
            date_dialgo.setText(date);
            btn_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            activity.setMax_water(0);
        }else {
            activity.setMax_water(1);
        }


        if (progressBar.getProgress() == 0) {
            none_data.setVisibility(View.VISIBLE);
        } else {
            if (progressBar.getProgress() < 50){

            }else {

            }
            none_data.setVisibility(View.GONE);
        }

        if (activity.getAlramStatus()){
            activity.setAlramOn();
            switch_remider.setImageResource(R.drawable.ani_switch07);
        }else {
            activity.setAlramOf();
            switch_remider.setImageResource(R.drawable.ani_switch02);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.switch_remider:
                if (activity.getAlramStatus()){
                    switch_remider.setImageResource(R.drawable.ani_switch02);
                    activity.setAlramOf();
                    aBoolean = true;
                }else {
                    switch_remider.setImageResource(R.drawable.ani_switch07);
                    activity.setAlramOn();
                    aBoolean = false;
                }

                break;
            case R.id.closeView:
                ll_custom_size.setVisibility(View.GONE);
                btn_add_option.setVisibility(View.VISIBLE);
                closeView.setVisibility(View.GONE);
                break;

            case R.id.btn_custom_size:
                new DialogCustomWater.Builder(activity)
                        .isCancellable(true)
                        .setpath(activity.pathfileIconWaterAssets)
                        .setHomeActivity(activity)
                        .setContext(MyApplication.getAppContext())
                        .setvalue("300")
                        .setSebEven(this)
                        .build();
                break;

            case R.id.select_day:
                activity.showDatePicker();
                break;

            case R.id.btn_ago_day:
                if (keyago == 0) {
                    btn_ago_day.setVisibility(View.GONE);
                    btn_nextday.setVisibility(View.VISIBLE);
                    int ago_day = (activity.getDay() - 1);
                    String newagoday;
                    if (ago_day < 10) {
                        newagoday = "0" + ago_day;
                    } else {
                        newagoday = ago_day + "";
                    }
                    String day_ago = (newagoday + "-" + activity.getMonth() + "-" + activity.getYear());
                    select_day.setText("Yesterday");

                    date = day_ago;
                    activity.setbackupday(date);
                    arrayList_water_day.clear();
                    arrayList_water_day.addAll(dbManager.getWaterDay(date));
                    if (arrayList_water_day.size() == 0) {
                        dbManager.addWaterDay(new Entity_Water_Day(0, date, activity.getWeigh() * 33, Item_kg.kg));
                    }
                    getDataWhereDate(day_ago);

                } else {
                    btn_ago_day.setVisibility(View.VISIBLE);
                    btn_nextday.setVisibility(View.GONE);
                    String day_next = (activity.getDate());
                    select_day.setText("Today");

                    date = day_next;
                    keyago = 0;
                    arrayList_water_day.clear();
                    arrayList_water_day.addAll(dbManager.getWaterDay(date));
                    if (arrayList_water_day.size() == 0) {
                        dbManager.addWaterDay(new Entity_Water_Day(0, date, activity.getWeigh() * 33, Item_kg.kg));
                    }
                    getDataWhereDate(day_next);
                    activity.setbackupday(date);
                }

                break;

            case R.id.btn_nextday:
                btn_ago_day.setVisibility(View.VISIBLE);
                btn_nextday.setVisibility(View.GONE);
                String day_next = (activity.getDate());
                select_day.setText("Today");
                getDataWhereDate(day_next);
                date = day_next;
                arrayList_water_day.clear();
                arrayList_water_day.addAll(dbManager.getWaterDay(date));
                if (arrayList_water_day.size() == 0) {
                    dbManager.addWaterDay(new Entity_Water_Day(0, date, activity.getWeigh() * 33, Item_kg.kg));
                }
                activity.setbackupday(date);
                break;
            case R.id.btn_total_water:

                new DialogEditTotalWater.Builder(activity).setContext(MyApplication.getAppContext()).setOnclickupdate(this::onClickUpdateWater).show();

                break;

        }
    }

    @Override
    public void sendevent() {
        arrayListSizeWater.clear();
        arrayListSizeWater.addAll(dbManager.getSizeWaterDB());
        adapter_water.notifyDataSetChanged();


    }

    @Override
    public void longClickItem(int possion) {
        arrayListSizeWater.clear();
        arrayListSizeWater.addAll(dbManager.getSizeWaterDB());
        adapter_water.notifyDataSetChanged();
    }

    /*
     Onclick item add water
     */
    @Override
    public void onclickwater(int possison, String path, String totalwater) {
        dbManager.addWater(new Entity_water(0, activity.getTime(), date, path, totalwater));
        getDataWhereDate(date);
//        progressBar.setProgress(activity.getWaterday(date));
    }

    @Override
    public void onclclickWaterSale(Entity_water entity_water) {
        new DialogEditWater.Builder(activity)
                .setCopyWater(this)
                .setContext(MyApplication.getAppContext()).setData(entity_water).buid();
    }

    @Override
    public void copyWater(Entity_water entity_water) {
        dbManager.addWater(entity_water);
        getDataWhereDate(date);
    }

    @Override
    public void deleteWater(Entity_water entity_water) {
        dbManager.deleteWater(entity_water.getId());
        getDataWhereDate(date);
    }

    @Override
    public void okWater(Entity_water entity_water) {
        dbManager.Update(entity_water);
        getDataWhereDate(date);
    }

    // get data in datepickerdialog
    @Override
    public void getdatepicker(String datepicker, int daypicker, int month, int yeah) {
        select_day.setText(datepicker);
        date = datepicker;

        if (activity.getDay() == daypicker) {
            select_day.setText("Today");
            btn_nextday.setVisibility(View.GONE);
            btn_ago_day.setVisibility(View.VISIBLE);
        } else if (activity.getDay() < daypicker) {
            btn_ago_day.setVisibility(View.VISIBLE);
            btn_nextday.setVisibility(View.GONE);
            keyago = 1;
        } else if (activity.getDay() > daypicker) {
            btn_ago_day.setVisibility(View.GONE);
            btn_nextday.setVisibility(View.VISIBLE);
        }
        if (arrayList_water_day.size() == 0) {
            dbManager.addWaterDay(new Entity_Water_Day(0, date, activity.getWeigh() * 33, Item_kg.kg));
        }
        getDataWhereDate(date);

    }


    public void getDataWhereDate(String date) {
        arrayList_water.clear();
        arrayList_water.addAll(dbManager.gettWaterWhereDate(date));
        adapter_newwater.notifyDataSetChanged();
        txt_water_day.setText(activity.getWaterday(date) + "");
        setUpProget(date);


    }

    //UpdateWater
    @Override
    public void onClickUpdateWater(int total, String untinvalue) {
        dbManager.updateWaterDay(new Entity_Water_Day(0,activity.getBackUpDay(), total, untinvalue));
        water_total.setText(total + "");
        activity.setWaterofday(total);
        setUpProget(activity.getBackUpDay());

    }
}
