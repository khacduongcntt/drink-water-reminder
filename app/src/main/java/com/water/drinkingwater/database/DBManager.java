package com.water.drinkingwater.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.water.drinkingwater.object.EntityCustomSizeWater;
import com.water.drinkingwater.object.EntityWeight;
import com.water.drinkingwater.object.Entity_Water_Day;
import com.water.drinkingwater.object.Entity_water;

import java.util.ArrayList;

public class DBManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "custom_size_water";
    private static final String TABLE_NAME = "table_size_water";
    private static final String TABLE_WATER = "table_water";
    private static final String TABLE_WATER_DAY = "table_water_day";
    private static final String TABLE_WEIGHT = "table_weigh";
    private static final String ID_WEIGHT = "id";
    private static final String YEAH_WEIGHT = "yeah";
    private static final String DATE_MONTH_WEIGHT = "date_month";
    private static final String WEIGHT = "weight";
    private static final String ID_WATER_DAY = "id";
    private static final String DATE_WATER_DAY = "date";
    private static final String WATER_DAY = "water";
    private static final String VALUE_DAY = "value";
    private static final int VERSION = 1;
    private static final String ID = "id";
    private static final String ID_WATER = "id";
    private static final String TIME_WATER = "time_water";
    private static final String DATE_WATER = "date_water";
    private static final String IMG_WATER = "img_water";
    private static final String TOTAL_WATER = "total_water";
    private static final String PATH_ICON = "path_icon";
    private static final String SIZE_WATER = "size_water";
    String sql = "CREATE TABLE table_size_water (\n" +
            "    id         INTEGER         PRIMARY KEY AUTOINCREMENT,\n" +
            "    path_icon  STRING (999999),\n" +
            "    size_water STRING (9999) \n" +
            ");\n";

    String sql_water = "CREATE TABLE table_water (\n" +
            "    id          INTEGER       PRIMARY KEY AUTOINCREMENT,\n" +
            "    time_water  STRING (9999),\n" +
            "    date_water  STRING (9999),\n" +
            "    img_water   STRING (9999),\n" +
            "    total_water STRING (9999) \n" +
            ");\n";
    String sql_water_day = "CREATE TABLE table_water_day (\n" +
            "    id    INTEGER          PRIMARY KEY AUTOINCREMENT,\n" +
            "    date  STRING (9999),\n" +
            "    water INTEGER (999999),\n" +
            "    value STRING (999) \n" +
            ");\n";

    String sql_weight = "CREATE TABLE table_weigh (\n" +
            "    id         INTEGER          PRIMARY KEY AUTOINCREMENT,\n" +
            "    yeah       STRING (9999),\n" +
            "    date_month STRING (9999),\n" +
            "    weight     STRING (9999999) \n" +
            ");\n";

    public DBManager(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sql);
        db.execSQL(sql_water);
        db.execSQL(sql_water_day);
        db.execSQL(sql_weight);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
        String sql_water = "DROP TABLE IF EXISTS " + TABLE_WATER;
        String sql_water_day = "DROP TABLE IF EXISTS " + TABLE_WATER_DAY;
        String sql_weight = "DROP TABLE IF EXISTS " + TABLE_WEIGHT;
        db.execSQL(sql);
        db.execSQL(sql_water_day);
        db.execSQL(sql_water);
        db.execSQL(sql_weight);
        onCreate(db);
    }


    public ArrayList<EntityWeight> getWeight(String yeah) {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<EntityWeight> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_weigh WHERE yeah =" + "'" + yeah + "'";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            EntityWeight entityWeight = new EntityWeight();
            entityWeight.setId(cursor.getInt(0));
            entityWeight.setYeah(cursor.getString(1));
            entityWeight.setDate_month(cursor.getString(2));
            entityWeight.setWeight(cursor.getFloat(3));
            arrayList.add(entityWeight);
        }
        return arrayList;
    }
    public ArrayList<EntityWeight> getWeightAll() {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<EntityWeight> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_weigh";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            EntityWeight entityWeight = new EntityWeight();
            entityWeight.setId(cursor.getInt(0));
            entityWeight.setYeah(cursor.getString(1));
            entityWeight.setDate_month(cursor.getString(2));
            entityWeight.setWeight(cursor.getFloat(3));
            arrayList.add(entityWeight);
        }
        return arrayList;
    }

    public void addWeight(EntityWeight entityWeight) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(YEAH_WEIGHT, entityWeight.getYeah());
        contentValues.put(DATE_MONTH_WEIGHT, entityWeight.getDate_month());
        contentValues.put(WEIGHT, entityWeight.getWeight());
        database.insert(TABLE_WEIGHT, null, contentValues);
    }

    public ArrayList<Entity_Water_Day> getWaterDay(String date) {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<Entity_Water_Day> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_water_day WHERE date =" + "'" + date + "'";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Entity_Water_Day entity_water_day = new Entity_Water_Day();
            entity_water_day.setId(cursor.getInt(0));
            entity_water_day.setDate(cursor.getString(1));
            entity_water_day.setWater(cursor.getInt(2));
            entity_water_day.setValue(cursor.getString(3));
            arrayList.add(entity_water_day);
        }
        return arrayList;
    }



    public ArrayList<Entity_Water_Day> getWaterDayNoW() {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<Entity_Water_Day> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_water_day";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Entity_Water_Day entity_water_day = new Entity_Water_Day();
            entity_water_day.setId(cursor.getInt(0));
            entity_water_day.setDate(cursor.getString(1));
            entity_water_day.setWater(cursor.getInt(2));
            entity_water_day.setValue(cursor.getString(3));
            arrayList.add(entity_water_day);
        }
        return arrayList;
    }



    public void addWaterDay(Entity_Water_Day entity_water_day) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DATE_WATER_DAY, entity_water_day.getDate());
        values.put(WATER_DAY, entity_water_day.getWater());
        values.put(VALUE_DAY, entity_water_day.getValue());
        database.insert(TABLE_WATER_DAY, null, values);

    }

    public int updateWaterDay(Entity_Water_Day entity_water_day) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATE_WATER_DAY, entity_water_day.getDate());
        contentValues.put(WATER_DAY, entity_water_day.getWater());
        contentValues.put(VALUE_DAY, entity_water_day.getValue());
        return database.update(TABLE_WATER_DAY, contentValues, DATE_WATER_DAY + " = ?", new String[]{String.valueOf(entity_water_day.getDate())});
    }

    public ArrayList<EntityCustomSizeWater> getSizeWaterDB() {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<EntityCustomSizeWater> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_size_water";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            EntityCustomSizeWater entityCustomSizeWater = new EntityCustomSizeWater();
            entityCustomSizeWater.setId(cursor.getInt(0));
            entityCustomSizeWater.setPath_icon(cursor.getString(1));
            entityCustomSizeWater.setSize_water(cursor.getString(2));
            arrayList.add(entityCustomSizeWater);

        }
        return arrayList;
    }

    public void addCustomsizeWater(EntityCustomSizeWater customSizeWater) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PATH_ICON, customSizeWater.getPath_icon());
        values.put(SIZE_WATER, customSizeWater.getSize_water());
        database.insert(TABLE_NAME, null, values);
    }

    public int deleteSizeWater(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        return database.delete(TABLE_NAME, ID + " =? ", new String[]{String.valueOf(id)});
    }


    public ArrayList<Entity_water> getWaterDB() {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<Entity_water> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_water";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Entity_water entity_water = new Entity_water();
            entity_water.setId(cursor.getInt(0));
            entity_water.setTime_water(cursor.getString(1));
            entity_water.setDate_water(cursor.getString(2));
            entity_water.setImg_water(cursor.getString(3));
            entity_water.setTotal_water(cursor.getString(4));
            arrayList.add(entity_water);
        }
        return arrayList;
    }

    public void addWater(Entity_water entity_water) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIME_WATER, entity_water.getTime_water());
        contentValues.put(DATE_WATER, entity_water.getDate_water());
        contentValues.put(IMG_WATER, entity_water.getImg_water());
        contentValues.put(TOTAL_WATER, entity_water.getTotal_water());
        database.insert(TABLE_WATER, null, contentValues);
    }

    public int deleteWater(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        return database.delete(TABLE_WATER, ID_WATER + " =? ", new String[]{String.valueOf(id)});
    }

    public ArrayList<Entity_water> gettWaterWhereDate(String date) {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<Entity_water> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_water WHERE date_water =" + "'" + date + "'";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Entity_water entity_water = new Entity_water();
            entity_water.setId(cursor.getInt(0));
            entity_water.setTime_water(cursor.getString(1));
            entity_water.setDate_water(cursor.getString(2));
            entity_water.setImg_water(cursor.getString(3));
            entity_water.setTotal_water(cursor.getString(4));
            arrayList.add(entity_water);
        }
        return arrayList;
    }


    public ArrayList<Entity_water> gettWaterWhere() {
        SQLiteDatabase database = this.getWritableDatabase();
        ArrayList<Entity_water> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM table_water";
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Entity_water entity_water = new Entity_water();
            entity_water.setId(cursor.getInt(0));
            entity_water.setTime_water(cursor.getString(1));
            entity_water.setDate_water(cursor.getString(2));
            entity_water.setImg_water(cursor.getString(3));
            entity_water.setTotal_water(cursor.getString(4));
            arrayList.add(entity_water);
        }
        return arrayList;
    }



    public int Update(Entity_water entity_water) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIME_WATER, entity_water.getTime_water());
        contentValues.put(DATE_WATER, entity_water.getDate_water());
        contentValues.put(IMG_WATER, entity_water.getImg_water());
        contentValues.put(TOTAL_WATER, entity_water.getTotal_water());
        return database.update(TABLE_WATER, contentValues, ID_WATER + " = ?", new String[]{String.valueOf(entity_water.getId())});
    }
}
