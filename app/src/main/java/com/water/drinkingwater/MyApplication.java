package com.water.drinkingwater;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {

    private static Context mContext;
    private static Activity mActivity;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mActivity = getActivity();
    }

    public static Context getAppContext() {
        return mContext;
    }
    public static Activity getActivity() {
        return mActivity;
    }
}