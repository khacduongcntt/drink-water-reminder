package com.water.drinkingwater.alram.service;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.water.drinkingwater.MainActivity;
import com.water.drinkingwater.MyApplication;
import com.water.drinkingwater.R;
import com.water.drinkingwater.alram.database.ReminderContract;
import com.water.drinkingwater.alram.database.ReminderItem;
import com.water.drinkingwater.alram.util.ReminderParams;
import com.water.drinkingwater.alram.util.ReminderType;
import com.water.drinkingwater.baseview.BaseActivity;
import com.water.drinkingwater.sharedPreferences.TinyDB;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static androidx.core.app.NotificationCompat.VISIBILITY_PUBLIC;
import static com.water.drinkingwater.baseview.BaseActivity.DATE_SETTING;
import static com.water.drinkingwater.baseview.BaseActivity.ITEM_CONTENT;
import static com.water.drinkingwater.baseview.BaseActivity.ITEM_TITLE;
import static com.water.drinkingwater.baseview.BaseActivity.REPEAT_MODES;
import static com.water.drinkingwater.baseview.BaseActivity.REPEAT_SETTING;
import static com.water.drinkingwater.baseview.BaseActivity.TIME_SETTING;

/**
 * Created by kyle on 07/09/16.
 */
public class AlarmReceiver extends BroadcastReceiver {

    private static final int HOURLY = 1, DAILY = 2, WEEKLY = 3, MONTHLY = 4, YEARLY = 5;
    BaseActivity baseActivity;
    public static Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getIntExtra(ReminderParams.ID, -1);
        String title = intent.getStringExtra(ReminderParams.TITLE);
        String msg = intent.getStringExtra(ReminderParams.CONTENT);
        mContext = context;
        if (context == null) {
            return;
        }

        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return;
        }

        Uri uri = ContentUris.withAppendedId(ReminderContract.All.CONTENT_URI, id);
        Cursor cursor = contentResolver.query(uri,
                null, null, null, null);

        if (cursor == null || !cursor.moveToFirst()) {
            return;
        }

        int frequency = cursor.getInt(cursor.getColumnIndex(ReminderParams.FREQUENCY));
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(ReminderParams.TIME)));
        cursor.close();

        if (frequency > 0) {
            if (frequency == HOURLY) {
                time.add(Calendar.HOUR, 1);

            } else if (frequency == DAILY) {
                time.add(Calendar.DATE, 1);

            } else if (frequency == WEEKLY) {
                time.add(Calendar.DATE, 7);
            } else if (frequency == MONTHLY) {
                time.add(Calendar.MONTH, 1);

            } else if (frequency == YEARLY) {
                time.add(Calendar.YEAR, 1);

            }

            ContentValues values = new ContentValues();
            values.put(ReminderContract.Alerts.TIME, time.getTimeInMillis());
            uri = ContentUris.withAppendedId(ReminderContract.Alerts.CONTENT_URI, id);
            context.getContentResolver().update(uri, values, null, null);

            Intent setAlarm = new Intent(context, AlarmService.class);
            setAlarm.putExtra(ReminderParams.ID, id);
            setAlarm.setAction("CREATE");
            context.startService(setAlarm);
        }

        TinyDB tinyDB = new TinyDB(context);

        boolean ischeck = tinyDB.getBoolean(BaseActivity.aBooleanRmenider,false);
        Log.e("duongnk", "onReceive: "+ischeck );
        if (ischeck){

            Calendar mcurrentTime = Calendar.getInstance();
            int h = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            Log.e("duongnk", "onReceive: "+h );
            int gio_di_ngu = tinyDB.getInt("gio_di_ngu",22);

            if (h==gio_di_ngu){
                createNotifications(context,"It's time for your bed");
            }else {
                if (h!=1&&h!=2&&h!=3&&h!=4&&h!=5&&h!=6&&h!=7&&h!=23&&h!=0&&h!=12) {
                    createNotifications(context, "its time to drink water");
                }
            }


        }else {

        }
        execute(id);

    }
    private void execute(int id) {

        AlarmManager alarm = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Uri uri = ContentUris.withAppendedId(ReminderContract.Alerts.CONTENT_URI,
                id);
        Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);

        if (cursor == null || !cursor.moveToFirst()) {
            return;
        }

        Intent intent = new Intent(mContext, AlarmReceiver.class);
        intent.putExtra(ReminderParams.ID, cursor.getInt(cursor.getColumnIndex(ReminderParams.ID)));
        intent.putExtra(ReminderParams.TITLE, cursor.getString(cursor.getColumnIndex(
                ReminderParams.TITLE)));
        intent.putExtra(ReminderParams.CONTENT, cursor.getString(cursor.getColumnIndex(
                ReminderParams.CONTENT)));

        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, id, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar rightNow = Calendar.getInstance();
        long timeInMilliseconds = rightNow.getTimeInMillis() + 3000000;


            alarm.setExact(AlarmManager.RTC_WAKEUP, timeInMilliseconds, pendingIntent);


        cursor.close();
    }
    public static void createNotifications(Context context,String messeger) {
        /**Get reference to notification manager*/
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        /**From Oreo we need to display notifications in the notification channel*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    "reminder_notification_channel", //String ID
                    "Alarm Manager", //Name for the channel
                    NotificationManager.IMPORTANCE_HIGH); //Importance for the notification , In high we get headsUp notification
            notificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "reminder_notification_channel")
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setSmallIcon(R.drawable.guide_ani_male)
                .setContentTitle("Drink Water Reminder") // Title
                .setContentText(messeger) //Text
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVisibility(VISIBILITY_PUBLIC)
                .setContentIntent(contentIntent(context)) //pending Intent (check its fn)
                .setAutoCancel(true); //Notification will go away when user clicks on it

        /**this will give heads up notification on versions below Oreo*/
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
        }

        //AlARM_REMINDER_NOTIFICATION_ID -> this ID can be used if the notification have to ba cancelled


        notificationManager.notify(213213, notificationBuilder.build());

    }

    private static PendingIntent contentIntent(Context context) {
        Intent startActivityIntent = new Intent(context, MainActivity.class);
        return PendingIntent.getActivity(context,
                213213,
                startActivityIntent,
                //FLAG_UPDATE_CURRENT -> keeps this instance valid and just updates the extra data associated with it coming from new intent
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
