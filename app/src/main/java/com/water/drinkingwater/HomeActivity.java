package com.water.drinkingwater;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;


import com.water.drinkingwater.baseview.BaseActivity;
import com.water.drinkingwater.fragment.Fragment_settings;
import com.water.drinkingwater.fragment.MasHomeNewFragment;
import com.water.drinkingwater.fragment.MastChartWaterFragment;
import com.water.drinkingwater.util.ControlSharePreferences;

public class HomeActivity extends BaseActivity implements View.OnClickListener {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    AppCompatImageView btn_chart, btn_water, btn_settings;
    RelativeLayout v_trailer;
    RelativeLayout v2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        v_trailer = findViewById(R.id.v_trailer);
        v2 = findViewById(R.id.v2);
        Animation logoMoveAnimation = AnimationUtils.loadAnimation(this, R.anim.design_bottom_sheet_slide_in);
        v2.setAnimation(logoMoveAnimation);

       Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(2500);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Animation    logoMoveAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.design_bottom_sheet_slide_out);
                                Animation logoMoveAnimation2 = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.design_bottom_sheet_slide_in);
                                v_trailer.setAnimation(logoMoveAnimation);
                                v_trailer.setVisibility(View.GONE);
                                v2.setVisibility(View.VISIBLE);
                                v2.setAnimation(logoMoveAnimation2);
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
        };
        thread.start();


        setischeckLogin(1);
        btn_chart = findViewById(R.id.btn_chart_v4);
        btn_water = findViewById(R.id.btn_water_v4);
        btn_settings = findViewById(R.id.btn_settings_v4);
        ControlSharePreferences controlSharePreferences = new ControlSharePreferences(HomeActivity.this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, new MasHomeNewFragment(HomeActivity.this));
        fragmentTransaction.commit();

        findViewById(R.id.btn_chart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_chart.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                btn_water.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.gray_2), android.graphics.PorterDuff.Mode.MULTIPLY);
                btn_settings.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.gray_2), android.graphics.PorterDuff.Mode.MULTIPLY);
                addFChart();


            }
        });
        findViewById(R.id.btn_water).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_chart.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.gray_2), android.graphics.PorterDuff.Mode.MULTIPLY);
                btn_water.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                btn_settings.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.gray_2), android.graphics.PorterDuff.Mode.MULTIPLY);
                addWater();

            }
        });
        findViewById(R.id.btn_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_chart.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.gray_2), android.graphics.PorterDuff.Mode.MULTIPLY);
                btn_water.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.gray_2), android.graphics.PorterDuff.Mode.MULTIPLY);
                btn_settings.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);


                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, new Fragment_settings(HomeActivity.this));
//                fragmentTransaction.addToBackStack("duongnk");
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    public void addFChart() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, new MastChartWaterFragment(HomeActivity.this));
//                fragmentTransaction.addToBackStack("duongnk");
        fragmentTransaction.commit();
    }

    public void addWater() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, new MasHomeNewFragment(HomeActivity.this));
//                fragmentTransaction.addToBackStack("duongnk");
        fragmentTransaction.commit();
    }

}
