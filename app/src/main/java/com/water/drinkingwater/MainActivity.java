package com.water.drinkingwater;

import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.water.drinkingwater.alram.database.ReminderItem;
import com.water.drinkingwater.baseview.BaseActivity;
import com.water.drinkingwater.baseview.mTextview;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.EntityWeight;
import com.water.drinkingwater.object.Entity_Water_Day;
import com.water.drinkingwater.object.Item;
import com.water.drinkingwater.object.Item_kg;
import com.water.drinkingwater.object.Item_lb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import top.defaults.view.PickerView;

public class MainActivity extends BaseActivity {


    @BindView(R.id.btn_reset_data)
    mTextview btn_reset_data;
    @BindView(R.id.ll_intro)
    LinearLayout ll_intro;
    @BindView(R.id.img_icon_intro)
    AppCompatImageView img_icon_intro;
    @BindView(R.id.ll_choose_gender)
    RelativeLayout ll_choose_gender;
    @BindView(R.id.ll_energy)
    LinearLayout ll_energy;
    @BindView(R.id.btn_lestgo)
    Button btn_lestgo;
    @BindView(R.id.btn_male)
    LinearLayout btn_male;
    @BindView(R.id.btn_female)
    LinearLayout btn_female;
    @BindView(R.id.icon_male)
    AppCompatImageView icon_male;
    @BindView(R.id.icon_female)
    AppCompatImageView icon_female;
    @BindView(R.id.txt_male)
    mTextview txt_male;
    @BindView(R.id.txt_female)
    mTextview txt_female;
    @BindView(R.id.txt_title_intro)
    mTextview txt_title_intro;
    @BindView(R.id.txt_description_intro)
    mTextview txt_description_intro;
    @BindView(R.id.ll_weigh)
    LinearLayout ll_weigh;
    @BindView(R.id.btn_skip)
    mTextview btn_skip;
    @BindView(R.id.pickerView_lb)
    PickerView pickerView_lb;
    int inext = 1;
    int gender = 0;
    boolean ischecktypeweigh = false;
    DBManager dbManager;
    private String mTime, mDate;
    private Calendar mAlertTime;
    private int mRepeatMode = 0;
    private Map<String, String> mAlarmTime, mAlarmDate, mAlarmRepeat;
    private static final String ITEM_TITLE = "header";
    private static final String ITEM_CONTENT = "content";

    private static final String TIME_SETTING = "Time";
    private static final String DATE_SETTING = "Date";
    private static final String REPEAT_SETTING = "Repeat";

    private static final int TIME_POSITION = 0;
    private static final int DATE_POSITION = 1;
    private static final int REPEAT_POSITION = 2;
    public static final int timeAlarmRepeat = 1800000;
    @OnClick({R.id.btn_lestgo})
    void clickLestgo() {
        if (inext == 1) {
            hideIntro();
            inext++;
            setmValueW("ml");
        } else if (inext == 2) {
            myWeigh(true);
            setSharePreferencesGender(gender);
            inext++;


        } else if (inext == 3) {
            if (ischecktypeweigh) {
                setSharePreferencesWeigh(ilB, Item_kg.lb);
                // add lượng nước của một ngày phải uống khi lần đầu tiên creat
                dbManager.addWaterDay(new Entity_Water_Day(0, getDate(), ilB*33,"fl oz"));
                // add cân nặng lần đầu tiên creat
                dbManager.addWeight(new EntityWeight(0,getYear()+"",getDay()+"/"+getMonth(),ilB));
            } else {
                setSharePreferencesWeigh(iKG, Item_kg.kg);
                // add lượng nước của một ngày phải uống khi lần đầu tiên creat
                dbManager.addWaterDay(new Entity_Water_Day(0, getDate(), iKG*33, "ml"));
                // add cân nặng lần đầu tiên creat
                dbManager.addWeight(new EntityWeight(0,getYear()+"",getDay()+"/"+getMonth(),iKG));
            }
            creatRemider();
            setAlramOn();
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick({R.id.btn_back})
    void clickback() {
        if (inext == 2) {
            displayIntro();
            inext--;
        } else if (inext == 3) {
            myWeigh(false);
            inext--;
        }
    }


    @OnClick(R.id.btn_skip)
    void onclickSkip() {
        if (ischecktypeweigh) {
            setSharePreferencesWeigh(ilB, Item_kg.lb);
            // add lượng nước của một ngày phải uống khi lần đầu tiên creat
            dbManager.addWaterDay(new Entity_Water_Day(0,getDate(),ilB*33,Item_kg.lb));
            // add cân nặng lần đầu tiên creat
            dbManager.addWeight(new EntityWeight(0,getYear()+"",getDay()+"/"+getMonth(),ilB));
        } else {
            setSharePreferencesWeigh(iKG, Item_kg.kg);
            // add lượng nước của một ngày phải uống khi lần đầu tiên creat
            dbManager.addWaterDay(new Entity_Water_Day(0,getDate(),iKG*33,Item_kg.kg));
            // add cân nặng lần đầu tiên creat
            dbManager.addWeight(new EntityWeight(0,getYear()+"",getDay()+"/"+getMonth(),iKG));
        }
        creatRemider();
        setAlramOn();
        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_male)
    void onclickMale() {
        changeGender(true);
    }

    @OnClick(R.id.btn_female)
    void onclickFemale() {
        changeGender(false);
    }

    @BindView(R.id.img_status_weigh)
    AppCompatImageView img_status_weigh;
    @BindView(R.id.pickerView)
    PickerView pickerView;
    @BindView(R.id.pickerView_kg_lg)
    PickerView pickerView_kg_lg;

    int iKG;
    int ilB;
    String itype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbManager = new DBManager(this);
        ButterKnife.bind(this);
        mAlertTime = Calendar.getInstance();
        List<Map<String, String>> mapList = new ArrayList<>();
        mAlarmTime = new HashMap<>();
        mAlarmDate = new HashMap<>();
        mAlarmRepeat = new HashMap<>();

        mAlarmTime.put(ITEM_TITLE, TIME_SETTING);
        mAlarmTime.put(ITEM_CONTENT, mTime);
        mAlarmDate.put(ITEM_TITLE, DATE_SETTING);
        mAlarmDate.put(ITEM_CONTENT, mDate);
        mAlarmRepeat.put(ITEM_TITLE, REPEAT_SETTING);
        mAlarmRepeat.put(ITEM_CONTENT, REPEAT_MODES[mRepeatMode]);

        mapList.add(mAlarmTime);
        mapList.add(mAlarmDate);
        mapList.add(mAlarmRepeat);


        configKG();
        if (getIscheckLogin()==1){
            Intent intent = new Intent(MainActivity.this,HomeActivity.class);
            startActivity(intent);
            finish();
        }


    }


    private void configKG() {
        pickerView.setItems(Item.sampleItems(), new PickerView.OnItemSelectedListener<Item>() {
            @Override
            public void onItemSelected(Item Item) {
                iKG = Item.getKg();
                if (iKG < 25) {
                    img_status_weigh.setImageResource(R.drawable.guide_ani_3_img2);
                } else if (iKG >= 25 && iKG < 100) {
                    img_status_weigh.setImageResource(R.drawable.guide_ani_3_img1);
                }

            }
        });
        pickerView.setSelectedItemPosition(50);
        pickerView.setSelectedItemPosition(4);
        pickerView.setTextSize(80);
        pickerView.setItemHeight(120);
        pickerView.setBackgroundColor(getResources().getColor(R.color.white));
        pickerView.setCyclic(false);
        pickerView.setTextColor(getResources().getColor(R.color.colorAccent));
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        pickerView.setTypeface(typeface);

        pickerView_lb.setItems(Item_lb.sampleItems(), new PickerView.OnItemSelectedListener<Item_lb>() {
            @Override
            public void onItemSelected(Item_lb item) {
                ilB = item.getKg();
                if (ilB < 25 * 2.2) {
                    img_status_weigh.setImageResource(R.drawable.guide_ani_3_img2);
                } else if (ilB >= 25 * 2.2 && ilB < 100 * 2.2) {
                    img_status_weigh.setImageResource(R.drawable.guide_ani_3_img1);
                }
            }
        });
        pickerView_lb.setSelectedItemPosition(4);
        pickerView_lb.setTextSize(80);
        pickerView_lb.setItemHeight(120);
        pickerView_lb.setBackgroundColor(getResources().getColor(R.color.white));
        pickerView_lb.setCyclic(false);
        pickerView_lb.setTextColor(getResources().getColor(R.color.colorAccent));
        pickerView_lb.setSelectedItemPosition(50);
        pickerView_lb.setTypeface(typeface);

        pickerView_kg_lg.setItems(Item_kg.sampleItemsTylpe(), new PickerView.OnItemSelectedListener<Item_kg>() {
            @Override
            public void onItemSelected(Item_kg item) {

                if (item.gettype().equals(Item_kg.kg)) {
                    pickerView.setVisibility(View.VISIBLE);
                    pickerView.setSelectedItemPosition(pickerView_lb.getSelectedItemPosition());
                    pickerView_lb.setVisibility(View.GONE);
                    ischecktypeweigh = false;
                } else {
                    pickerView_lb.setVisibility(View.VISIBLE);
                    pickerView_lb.setSelectedItemPosition(pickerView.getSelectedItemPosition());
                    pickerView.setVisibility(View.GONE);
                    ischecktypeweigh = true;
                }


            }
        });
        pickerView_kg_lg.setSelectedItemPosition(4);
        pickerView_kg_lg.setTextSize(60);
        pickerView_kg_lg.setItemHeight(120);
        pickerView_kg_lg.setBackgroundColor(getResources().getColor(R.color.white));
        pickerView_kg_lg.setCyclic(false);
        pickerView_kg_lg.setTextColor(getResources().getColor(R.color.colorAccent));
        pickerView_kg_lg.setTypeface(typeface);


    }

    private void hideIntro() {
        btn_reset_data.setVisibility(View.INVISIBLE);
        setanimation(btn_reset_data, false);
        ll_intro.setVisibility(View.GONE);
        setanimation(ll_intro, false);
        img_icon_intro.setVisibility(View.GONE);
        setanimation(img_icon_intro, false);
        ll_energy.setVisibility(View.VISIBLE);
        setanimation(ll_energy, true);
        ll_choose_gender.setVisibility(View.VISIBLE);
        setanimation(ll_choose_gender, true);
        btn_lestgo.setText("NEXT");

    }

    private void displayIntro() {
        btn_reset_data.setVisibility(View.VISIBLE);
        ll_intro.setVisibility(View.VISIBLE);
        img_icon_intro.setVisibility(View.VISIBLE);

        ll_energy.setVisibility(View.GONE);
        ll_choose_gender.setVisibility(View.GONE);
        btn_lestgo.setText("LET'S GO");
        setanimation(btn_reset_data, true);
        setanimation(ll_intro, true);
        setanimation(img_icon_intro, true);
        setanimation(ll_energy, false);
        setanimation(ll_choose_gender, false);
    }

    private void changeGender(boolean b) {
        if (b) {
            icon_female.setImageResource(R.drawable.female_no_choice);
            icon_male.setImageResource(R.drawable.guide_ani_male);
            txt_male.setTypeface(2);
            txt_female.setTypeface(1);
            txt_male.setTextColor(getResources().getColor(R.color.colorAccent));
            txt_female.setTextColor(Color.GRAY);
            gender = 0;
        } else {
            icon_female.setImageResource(R.drawable.guide_ani_female);
            icon_male.setImageResource(R.drawable.male_no_choice);
            txt_male.setTypeface(1);
            txt_female.setTypeface(2);
            txt_male.setTextColor(Color.GRAY);
            txt_female.setTextColor(getResources().getColor(R.color.colorAccent));
            gender = 1;
        }
    }

    private void myWeigh(boolean b) {
        if (b) {
            txt_title_intro.setText(getResources().getString(R.string.How_much_do_you_weigh));
            txt_description_intro.setText(getResources().getString(R.string.We_need_your));
            ll_energy.setVisibility(View.GONE);
            ll_weigh.setVisibility(View.VISIBLE);
            setanimation(ll_energy, false);
            setanimation(ll_weigh, true);
        } else {
            txt_title_intro.setText(getResources().getString(R.string.choose_your_gender));
            txt_description_intro.setText(getResources().getString(R.string.we_ll_provide_you_with_tailored_advice));
            ll_energy.setVisibility(View.VISIBLE);
            ll_weigh.setVisibility(View.GONE);
            setanimation(ll_energy, true);
            setanimation(ll_weigh, false);
        }
    }

    private void mytimewakeup() {

    }

    public void setanimation(View view, boolean isanimation) {
        Animation logoMoveAnimation;
        if (isanimation) {
            logoMoveAnimation = AnimationUtils.loadAnimation(this, R.anim.design_bottom_sheet_slide_in);
        } else {
            logoMoveAnimation = AnimationUtils.loadAnimation(this, R.anim.design_bottom_sheet_slide_out);
        }
        view.setAnimation(logoMoveAnimation);

    }
    private ReminderItem mData;
    public void creatRemider(){
        mAlertTime = Calendar.getInstance();
        mData = new ReminderItem();
        Calendar rightNow = Calendar.getInstance();
        int currentHourIn24Format = rightNow.get(Calendar.HOUR_OF_DAY);
        int minutes = rightNow.get(Calendar.MINUTE);
        Calendar current = Calendar.getInstance();
        mTime = TIME_FORMAT.format(current.getTime());
        mDate = DATE_FORMAT.format(current.getTime());
        mAlertTime.setTimeInMillis(current.getTimeInMillis());
        mData.setTitle("Test");
        mData.setContent("DuongnkAndroid");
        mData.setFrequency(mRepeatMode);

        mAlertTime.set(Calendar.HOUR_OF_DAY, currentHourIn24Format);
        mAlertTime.set(Calendar.MINUTE, minutes);
        mAlertTime.set(Calendar.SECOND, 0);
        mTime = TIME_FORMAT.format(mAlertTime.getTime());
        mAlarmTime.put(ITEM_CONTENT, mTime);
        long time = mAlertTime.getTimeInMillis()+12000;
        mData.setTimeInMillis(rightNow.getTimeInMillis()+Repeattime);
        saveAlert(mData);
    }
}
