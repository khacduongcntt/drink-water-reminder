package com.water.drinkingwater.baseview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.water.drinkingwater.HomeActivity;
import com.water.drinkingwater.R;
import com.water.drinkingwater.adapter.Adapter_sticker;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.EntityCustomSizeWater;

public class DialogCustomWater {
    public static class Builder {
        private String path;
        private Activity activity;
        private boolean cancel;
        private HomeActivity homeActivity;
        private Context context;
        private String value;
        Adapter_sticker adapter_sticker;
        DBManager dbManager;
        int posisonicon = 0;
        sendEvent sendEvent;
        Adapter_sticker.Onclickpossisonsticker onclickpossisonsticker = new Adapter_sticker.Onclickpossisonsticker() {
            @Override
            public void Onclicksticker(int posison) {
                posisonicon = posison;
            }
        };


        public Builder(Activity activity) {
            this.activity = activity;
        }

        public Builder setContext(Context context) {
            this.context = context;
            return this;
        }

        public Builder setvalue(String value) {
            this.value = value;
            return this;
        }

        public Builder setHomeActivity(HomeActivity homeActivity) {
            this.homeActivity = homeActivity;
            return this;
        }

        public Builder setpath(String path) {
            this.path = path;
            return this;
        }

        public Builder isCancellable(boolean cancel) {
            this.cancel = cancel;
            return this;
        }

        public Builder setSebEven(sendEvent sendEvent) {
            this.sendEvent = sendEvent;
            return this;
        }

        public DialogCustomWater build() {
            RecyclerView recyclerview_icon_water;
            EditText edt_weight;
            Button btn_add_water;
            dbManager = new DBManager(context);
            Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_water_dialog);
            recyclerview_icon_water = dialog.findViewById(R.id.recyclerview_icon_water);
            edt_weight = dialog.findViewById(R.id.edt_weight);
            btn_add_water = dialog.findViewById(R.id.btn_add_water);
            dialog.setCancelable(cancel);
            recyclerview_icon_water.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            adapter_sticker = new Adapter_sticker(homeActivity.loadacessSticker("water"), context, onclickpossisonsticker, path);
            recyclerview_icon_water.setAdapter(adapter_sticker);
            dialog.show();
            if (!value.isEmpty()) {
                edt_weight.setText(value);
            }
            btn_add_water.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbManager.addCustomsizeWater(new EntityCustomSizeWater( homeActivity.loadacessSticker("water").get(posisonicon), edt_weight.getText().toString()));
                    dialog.dismiss();
                    if (sendEvent!=null){
                        sendEvent.sendevent();
                    }
                }
            });

            return new DialogCustomWater();
        }
    }

    public interface sendEvent {
        void sendevent();
    }
}
