package com.water.drinkingwater.baseview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.water.drinkingwater.HomeActivity;
import com.water.drinkingwater.R;
import com.water.drinkingwater.database.DBManager;

public class DialogEditTotalWater {


    public static class Builder implements View.OnClickListener, DialogProcessbar.sendValueProcess {
        Context mContext;
        HomeActivity activity;
        DBManager dbManager;
        Dialog dialog;
        boolean ishecksp = false;
        boolean ischeckw, ischeckw2 = false;
        ImageView img_weather, img_sports;
        boolean istouch;
        int total;
        String unit_value;
        OnclickUpdateWater onclickUpdateWater;

        public Builder(HomeActivity homeActivity) {
            this.activity = homeActivity;
        }

        public Builder setContext(Context mContext) {
            this.mContext = mContext;
            return this;
        }
        public Builder setOnclickupdate(OnclickUpdateWater onclickupdate){
            this.onclickUpdateWater = onclickupdate;
            return this;
        }

        mTextview txt_weater_default, txt_adjust, txt_weather, txt_sports, txt_total;

        public DialogEditTotalWater show() {

            dbManager = new DBManager(mContext);
            Button btn_cancel, btn_ok;
            LinearLayout btn_sports, btn_weather, btn_adjustment;

            total = dbManager.getWaterDay(activity.getDate()).get(0).getWater();
            unit_value = " " + activity.getmValueW();
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_edit_total_water);
            btn_adjustment = dialog.findViewById(R.id.btn_adjustment);
            btn_sports = dialog.findViewById(R.id.btn_sports);
            btn_weather = dialog.findViewById(R.id.btn_weather);
            txt_weather = dialog.findViewById(R.id.txt_weather);
            txt_weater_default = dialog.findViewById(R.id.txt_weater_default);
            txt_weater_default.setText(dbManager.getWaterDay(activity.getDate()).get(0).getWater() + unit_value);
            txt_adjust = dialog.findViewById(R.id.txt_adjust);
            txt_sports = dialog.findViewById(R.id.txt_sports);
            txt_total = dialog.findViewById(R.id.txt_total);
            btn_cancel = dialog.findViewById(R.id.btn_cancel);
            img_sports = dialog.findViewById(R.id.img_sports);
            img_weather = dialog.findViewById(R.id.img_weather);
            btn_ok = dialog.findViewById(R.id.btn_ok);
            ischeckvalue();
            btn_adjustment.setOnClickListener(this);
            btn_sports.setOnClickListener(this);
            btn_weather.setOnClickListener(this);
            txt_sports.setOnClickListener(this);
            txt_total.setOnClickListener(this);
            btn_cancel.setOnClickListener(this);
            btn_ok.setOnClickListener(this);
            txt_total.setText(total + unit_value);
            dialog.show();

            return new DialogEditTotalWater();
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_cancel:
                    dialog.dismiss();
                    break;
                case R.id.btn_ok:

                    dialog.dismiss();
                    if (onclickUpdateWater!=null){
                        onclickUpdateWater.onClickUpdateWater(total,unit_value);
                    }
                    break;


                case R.id.btn_weather:
                    istouch = true;
                    if (ischeckw) {

                        img_weather.setImageResource(R.drawable.checkbox_normal);
                        ischeckw = false;

                    } else {

                        img_weather.setImageResource(R.drawable.check);
                        new DialogProcessbar.Builder(activity).setSenProcess(this).setProcess(dbManager.getWaterDay(activity.getDate()).get(0).getWater()).build();
                        ischeckw = true;
                    }


                    break;

                case R.id.btn_sports:
                    istouch = false;
                    if (ischeckw2) {

                        img_sports.setImageResource(R.drawable.checkbox_normal);
                        ischeckw2 = false;

                    } else {

                        img_sports.setImageResource(R.drawable.check);
                        new DialogProcessbar.Builder(activity).setSenProcess(this).setProcess(dbManager.getWaterDay(activity.getDate()).get(0).getWater()).build();
                        ischeckw2 = true;
                    }
                    break;
            }
        }

        @Override
        public void senvalue(int value, int callback) {
            if (callback == 1) {
                if (istouch) {
                    txt_weather.setText("+ " + value + unit_value);
                    total = total + value;
                    txt_total.setText(total + "");
                    activity.setmWeather(value);
                } else {
                    txt_sports.setText("+ " + value + unit_value);
                    total = total + value;
                    txt_total.setText("+ " + total + unit_value);
                    activity.setmSport(value);
                }
            } else {
                if (istouch) {
                    img_weather.setImageResource(R.drawable.checkbox_normal);
                } else {
                    img_sports.setImageResource(R.drawable.checkbox_normal);

                }
            }
        }

        private void ischeckvalue() {
            if (activity.getmSport() != 0) {
                img_sports.setImageResource(R.drawable.check);
                txt_sports.setText(activity.getmSport() + unit_value);
                total = total + activity.getmSport();
                activity.setWaterofday(total);
            } else {
                img_sports.setImageResource(R.drawable.checkbox_normal);
                txt_sports.setText(activity.getmSport() + unit_value);
                total = total + activity.getmSport();
                activity.setWaterofday(total);
            }
            if (activity.getmWeather() != 0) {
                img_weather.setImageResource(R.drawable.check);
                txt_weather.setText(activity.getmWeather() + unit_value);
                total = total + activity.getmWeather();
                activity.setWaterofday(total);
            } else {
                img_weather.setImageResource(R.drawable.checkbox_normal);
                txt_weather.setText(activity.getmWeather() + unit_value);
                total = total + activity.getmSport();
                activity.setWaterofday(total);
            }
        }
    }

    public interface OnclickUpdateWater {
        void onClickUpdateWater(int total,String untnvalue);
    }
}
