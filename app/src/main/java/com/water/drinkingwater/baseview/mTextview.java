package com.water.drinkingwater.baseview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.water.drinkingwater.R;

@SuppressLint("AppCompatCustomView")

public class mTextview extends TextView {
    int styleTextView = 3;

    public mTextview(Context context) {
        super(context);
        setTypeface(context, null);
    }

    public mTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(context, attrs);
    }

    public mTextview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(context, attrs);
    }
    public void setTypeface(int type){
        Typeface typeface = null;
        if (type == 3) {
            typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");
        } else if (type == 1) {
            typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Light.ttf");
        } else if (type == 2) {
            typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");

        }
        this.setTypeface(typeface);
    }

    public void setTypeface(Context context, AttributeSet attributeSet) {
        Typeface typeface = null;
        TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.mTextview);
        styleTextView = a.getInt(R.styleable.mTextview_setStyleTextview, 1);
        if (attributeSet != null) {

            if (styleTextView == 3) {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");
            } else if (styleTextView == 1) {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Light.ttf");
            } else if (styleTextView == 2) {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");

            }
            this.setTypeface(typeface);

        }
    }

}
