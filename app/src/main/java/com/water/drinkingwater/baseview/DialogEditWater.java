package com.water.drinkingwater.baseview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.water.drinkingwater.HomeActivity;
import com.water.drinkingwater.R;
import com.water.drinkingwater.object.Entity_water;

public class DialogEditWater {


    public static class Builder implements BaseActivity.getTimePicker {
        Entity_water entity_water;
        HomeActivity activity;
        Context context;
        copyWater copyWater;
        String time;


        public Builder(HomeActivity activity) {
            this.activity = activity;
            activity.creatModelTime(this);
        }

        public Builder setData(Entity_water data) {
            this.entity_water = data;
            return this;
        }

        public Builder setContext(Context context) {
            this.context = context;
            return this;
        }

        public Builder setCopyWater(copyWater copyWater) {
            this.copyWater = copyWater;
            return this;
        }
        UITextViewUnderlineCustom txt_time;
        public DialogEditWater buid() {

            AppCompatImageView img_icon, btn_copy, btn_delete;
            EditText edt_weight;

            Button btn_cancel, btn_ok;
            Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_edit_water);

            img_icon = dialog.findViewById(R.id.img_icon);
            edt_weight = dialog.findViewById(R.id.edt_weight);
            txt_time = dialog.findViewById(R.id.txt_time);
            btn_copy = dialog.findViewById(R.id.btn_copy);
            btn_delete = dialog.findViewById(R.id.btn_delete);
            btn_cancel = dialog.findViewById(R.id.btn_cancel);
            btn_ok = dialog.findViewById(R.id.btn_ok);
            Glide.with(context).load(entity_water.getImg_water()).into(img_icon);
            edt_weight.setText(entity_water.getTotal_water());
            txt_time.setText(entity_water.getTime_water());
            time = entity_water.getTime_water();
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            txt_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.showTimePickerDialog();
                }
            });
            btn_copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    copyWater.copyWater(entity_water);
                }
            });
            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    copyWater.deleteWater(entity_water);
                }
            });
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    copyWater.okWater(new Entity_water(entity_water.getId(),time, entity_water.getDate_water(), entity_water.getImg_water(), edt_weight.getText().toString()));
                    dialog.dismiss();
                }
            });

            dialog.show();


            return new DialogEditWater();
        }


        @Override
        public void gettimePicker(String timepicker,int hour) {
            txt_time.setText(timepicker);
            time = timepicker;
        }
    }

    public interface copyWater {
        void copyWater(Entity_water entity_water);

        void deleteWater(Entity_water entity_water);

        void okWater(Entity_water entity_water);
    }
}
