package com.water.drinkingwater.baseview;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;


import com.water.drinkingwater.FileAcces;
import com.water.drinkingwater.MyApplication;
import com.water.drinkingwater.alram.database.ReminderContract;
import com.water.drinkingwater.alram.database.ReminderItem;

import com.water.drinkingwater.alram.service.AlarmService;
import com.water.drinkingwater.alram.util.ReminderParams;
import com.water.drinkingwater.alram.util.ReminderType;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.Entity_water;
import com.water.drinkingwater.sharedPreferences.TinyDB;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class BaseActivity extends AppCompatActivity {

    Context context;
    public final String pathfileIconWaterAssets = "file:///android_asset/water/";
    public static final String mGender = "gender";
    public static final String mWeigh = "weigh";
    public static final String mType = "typegender";
    public static final String mWeather = "weather";
    public static final String mSport = "sport";
    public static final String mValueW = "value_w";
    public static final String ischecklogin = "ischecklogin";
    public static final String aBooleanRmenider = "remider";
    public static final String backupday = "backupday";
    public static final String giodi_ngu = "gio_di_ngu";
    public static  final String max_water = "max_water";
    TinyDB tinyDB;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String showdatepicker;
    getdatePicker getdatePicker;
    getTimePicker getTimePicker;
    DBManager dbManager;
    private ContentResolver mContentResolver;
    private Map<String, String> mAlarmTime, mAlarmDate, mAlarmRepeat;
    private ReminderItem mData;
    private int mRepeatMode;
    private static final String NONE = "None";
    private static final String HOURLY = "Hourly";
    private static final String DAILY = "Daily";
    private static final String WEEKLY = "Weekly";
    private static final String MONTHLY = "Monthly";
    private static final String YEARLY = "Yearly";
    public static final String[] REPEAT_MODES =
            new String[]{NONE, HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY};

    public static final String ITEM_TITLE = "header";
    public static final String ITEM_CONTENT = "content";

    public static final String TIME_SETTING = "Time";
    public static final String DATE_SETTING = "Date";
    public static final String REPEAT_SETTING = "Repeat";

    private static final int TIME_POSITION = 0;
    private static final int DATE_POSITION = 1;
    private static final int REPEAT_POSITION = 2;
    public static final int Repeattime = 3000000;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseActivity.this;
        tinyDB = new TinyDB(context);
        dbManager = new DBManager(context);
//        mContentResolver = getContentResolver();
    }
    public void setischeckLogin(int i){
        tinyDB.putInt(ischecklogin, i);
    }
    public int getIscheckLogin(){
         return tinyDB.getInt(ischecklogin, 0);
    }

    public void creatModel(getdatePicker getdatePicker) {
        this.getdatePicker = getdatePicker;
    }

    public void creatModelTime(getTimePicker getTimePicker) {
        this.getTimePicker = getTimePicker;
    }


    public ArrayList<String> loadacessSticker(final String pathAcsset) {

        ArrayList<String> arr_icon = new ArrayList<>();
        FileAcces fileAccesHPBD = new FileAcces();
        arr_icon = fileAccesHPBD.getImage(context, pathAcsset);

        return arr_icon;
    }

    public void setSharePreferencesGender(int keyGender) {
        tinyDB.putInt(mGender, keyGender);
    }

    public void setSharePreferencesWeigh(int weigh, String type) {
        tinyDB.putString(mType, type);
        tinyDB.putInt(mWeigh, weigh);
    }


    public int getSharePreferencesGender() {
        return tinyDB.getInt(mGender, 0);
    }

    public int getWeigh() {

        return tinyDB.getInt(mWeigh, 55);
    }

    public String getTypeWeigh() {

        return tinyDB.getString(mType);
    }
    public void setMax_water(int i){
        tinyDB.putInt(max_water,i);
    }


    public void setmWeather(int value) {
        tinyDB.putInt(mWeather, value);
    }

    public void setmSport(int value) {
        tinyDB.putInt(mSport, value);
    }

    public int getmWeather() {
        return tinyDB.getInt(mWeather, 0);
    }

    public int getmSport() {
        return tinyDB.getInt(mSport, 0);
    }

    public void setmValueW(String value) {
        tinyDB.putString(mValueW,value);
    }
    public String getmValueW(){
        return tinyDB.getString(mValueW);
    }

    public String getDate() {

        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        return currentDate;

    }

    public String getTime() {
        String currentTime = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new Date());

        return currentTime;
    }

    public void setmHourBed(int hour){
        tinyDB.putInt(giodi_ngu,hour);
    }
    public int getmHourBed(){
        return tinyDB.getInt(giodi_ngu,22);
    }
    public void showTimePickerDialog() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(BaseActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mcurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                mcurrentTime.set(Calendar.MINUTE, selectedMinute);
                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm a");
                String time = timeformat.format(mcurrentTime.getTime());
                int h = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                getTimePicker.gettimePicker(time,h);
            }
        }, hour, minute, false);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void showDatePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (dayOfMonth < 10) {
                            showdatepicker = "0" + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        } else {
                            showdatepicker = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        }


                        getdatePicker.getdatepicker(showdatepicker, dayOfMonth,monthOfYear+1,year);


                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();


    }

    public String getDatePicker() {

        return showdatepicker;
    }

    public interface getdatePicker {
        void getdatepicker(String datepicker, int daypicker,int month,int yeah);
    }

    public interface getTimePicker {
        void gettimePicker(String timepicker,int hour);
    }

    public int getDay() {
        String day = new SimpleDateFormat("dd", Locale.getDefault()).format(new Date());
        String newday;
        int getday = Integer.parseInt(day);
        if (getday < 10) {
            newday = "0" + getday;
        } else {
            newday = getday + "";
        }
        return Integer.parseInt(newday);

    }

    public int getMonth() {
        String currentDate = new SimpleDateFormat("MM", Locale.getDefault()).format(new Date());
        return Integer.parseInt(currentDate);
    }

    public int getYear() {
        String currentDate = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
        return Integer.parseInt(currentDate);
    }

    public int getWaterofday() {
        int weigh = tinyDB.getInt(mWeigh, 55);
        int value = weigh * 33;
        return value;
    }
    public void setWaterofday(int value){
        tinyDB.putInt(mWeigh,value);
    }

    public int getWaterDay() {
        int water = dbManager.getWaterDay(getDate()).get(0).getWater();
        int value = water * 33;
        return water;
    }

    public int getWaterday(String date) {
        int waterday = 0;
        ArrayList<Entity_water> arrayList = new ArrayList<>();
        DBManager dbManager = new DBManager(MyApplication.getAppContext());
        arrayList.addAll(dbManager.gettWaterWhereDate(date));
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                waterday += Integer.parseInt(arrayList.get(i).getTotal_water());
            }
        }
        return waterday;
    }
    public void createAlarm(int id) {
        Intent alarm = new Intent(this, AlarmService.class);
        alarm.putExtra(ReminderParams.ID, id);
        alarm.setAction(AlarmService.CREATE);
        startService(alarm);
    }
    public void deleteAlert(ReminderItem item) {
        mContentResolver = getContentResolver();
        if (item != null) {
            Intent delete = new Intent(this, AlarmService.class);
            delete.putExtra(ReminderParams.ID, item.getId());
            delete.setAction(AlarmService.DELETE);
            Uri uri = ContentUris.withAppendedId(ReminderContract.Notes.CONTENT_URI, item.getId());
            mContentResolver.delete(uri, null, null);
            startService(delete);
            terminateActivity();
        } else {
            terminateActivity();
        }
    }
    private void terminateActivity() {
        NavUtils.navigateUpFromSameTask(this);
    }
    public void saveAlert(final ReminderItem item) {
        mContentResolver = getContentResolver();
        if (item.getId() > 0) {
            Intent cancelPrevious = new Intent(this,
                    AlarmService.class);
            cancelPrevious.putExtra(ReminderParams.ID, item.getId());
            cancelPrevious.setAction(AlarmService.CANCEL);
            startService(cancelPrevious);
            ContentValues values = new ContentValues();
            values.put(ReminderContract.Alerts.TITLE, item.getTitle());
            values.put(ReminderContract.Alerts.CONTENT, item.getContent());
            values.put(ReminderContract.Alerts.TIME, item.getTimeInMillis());
            values.put(ReminderContract.Alerts.FREQUENCY, item.getFrequency());
            Uri uri = ContentUris.withAppendedId(ReminderContract.Alerts.CONTENT_URI, item.getId());
            mContentResolver.update(uri, values, null, null);
            createAlarm(item.getId());
        } else {
            ContentValues values = new ContentValues();
            values.put(ReminderContract.Alerts.TYPE, ReminderType.ALERT.getName());
            values.put(ReminderContract.Alerts.TITLE, item.getTitle());
            values.put(ReminderContract.Alerts.CONTENT, item.getContent());
            values.put(ReminderContract.Alerts.TIME, item.getTimeInMillis());
            values.put(ReminderContract.Alerts.FREQUENCY, item.getFrequency());
            Uri uri = mContentResolver.insert(ReminderContract.Notes.CONTENT_URI,
                    values);
            if (uri != null) {
                createAlarm(Integer.parseInt(uri.getLastPathSegment()));
            }
        }
    }

    public AlertDialog createRepeatDialog() {
        final int prevRepeat = mRepeatMode;
        return new AlertDialog.Builder(this)
                .setTitle("Reapet")
                .setSingleChoiceItems(REPEAT_MODES, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        mRepeatMode = i;
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        mAlarmRepeat.put(ITEM_CONTENT, REPEAT_MODES[mRepeatMode]);
                        mData.setFrequency(mRepeatMode);
//                        mAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        mRepeatMode = prevRepeat;
                        mData.setFrequency(mRepeatMode);
                    }
                })
                .create();
    }
    public AlertDialog createDeleteDialog(final ReminderItem item) {
        return new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Delete")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        deleteAlert(item);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();
    }


    private Calendar mAlertTime;
    private String mTime, mDate;
    public static final DateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm aa", Locale.CANADA);
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yy", Locale.CANADA);
    public void creatRemider(){
        mAlertTime = Calendar.getInstance();
        List<Map<String, String>> mapList = new ArrayList<>();
        mAlarmTime = new HashMap<>();
        mAlarmDate = new HashMap<>();
        mAlarmRepeat = new HashMap<>();

        mAlarmTime.put(ITEM_TITLE, TIME_SETTING);
        mAlarmTime.put(ITEM_CONTENT, mTime);
        mAlarmDate.put(ITEM_TITLE, DATE_SETTING);
        mAlarmDate.put(ITEM_CONTENT, mDate);
        mAlarmRepeat.put(ITEM_TITLE, REPEAT_SETTING);
        mAlarmRepeat.put(ITEM_CONTENT, REPEAT_MODES[mRepeatMode]);

        mapList.add(mAlarmTime);
        mapList.add(mAlarmDate);
        mapList.add(mAlarmRepeat);

        mAlertTime = Calendar.getInstance();
        mData = new ReminderItem();
        Calendar rightNow = Calendar.getInstance();
        int currentHourIn24Format = rightNow.get(Calendar.HOUR_OF_DAY);
        int minutes = rightNow.get(Calendar.MINUTE);
        Calendar current = Calendar.getInstance();
        mTime = TIME_FORMAT.format(current.getTime());
        mDate = DATE_FORMAT.format(current.getTime());
        mAlertTime.setTimeInMillis(current.getTimeInMillis());
        mData.setTitle("Test");
        mData.setContent("DuongnkAndroid");
        mData.setFrequency(mRepeatMode);

        mAlertTime.set(Calendar.HOUR_OF_DAY, currentHourIn24Format);
        mAlertTime.set(Calendar.MINUTE, minutes);
        mAlertTime.set(Calendar.SECOND, 0);
        mTime = TIME_FORMAT.format(mAlertTime.getTime());
        mAlarmTime.put(ITEM_CONTENT, mTime);
        mData.setTimeInMillis(rightNow.getTimeInMillis()+120000);
        saveAlert(mData);
    }

    public void setAlramOn(){
        tinyDB.putBoolean(aBooleanRmenider,true);
    }
    public void setAlramOf(){
        tinyDB.putBoolean(aBooleanRmenider,false);
    }
    public boolean getAlramStatus(){
        return  tinyDB.getBoolean(aBooleanRmenider,false);
    }

    public void setbackupday(String day){
        tinyDB.putString(backupday,day);
    }
    public String getBackUpDay()

    {
        return tinyDB.getString(backupday);
    }
}
