package com.water.drinkingwater.baseview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.water.drinkingwater.MyApplication;
import com.water.drinkingwater.R;

/**
 * creat by duongnk
 * UIDialogImageCustom creat 13/9/2019
 * <p>
 * new UIDialogImageCustom.Builder(MainActivity.this)
 * .setImgDialog(R.drawable.myDrawable)  // set hình ảnh cho dialog
 * .setTitle("myTitle") // set Title cho dialog
 * .setTextColorMessage(MyColor) // set color Text messeage
 * .setMessage("myMessage")        // setMessage cho dialog
 * .setTextColorTitle(MyColor) // set Color text title
 * .isCancellable(true)
 * .setContentButtonDismis("Xác nhận") // set nội dung cho buttondialog 1.
 * .setContentButtonSubmit("Yêu cầu")  //  set nội dung cho buttondialog 2.
 * .setButtonDismisListener(new UIDialogImageCustom.UIDialogCustomListener() { // set onclick cho button1
 *
 * @Override public void onClick(Dialog dialog) {
 * My event button 1.
 * }
 * })
 * .setButtonSubmisListerner(new UIDialogImageCustom.UIDialogCustomListener() { // set onclick cho button2
 * @Override public void onClick(Dialog dialog) {
 * My event button 2.
 * }
 * })
 * .build();
 * <p>
 * Lưu ý: tùy từng các View mà chuyền các tham số.. tham số nào k sử dụng k cần chuyền vào dialog.
 */

public class UIDialogImageCustom {

    public static class Builder {
        private String title, mesage, content_button_one, content_button_two;
        private Integer img_dialog;
        private Activity activity;
        private UIDialogCustomListener btListenerOne, btListenerTwo;
        private boolean cancel;
        private Integer colorTitle, colorMessage;
        private Integer backgroudDismis;
        private Integer backgroudSubmis;
        private Integer orientation;
        private String messgaehtml;
        private Integer color,colorDismis;

        public Builder(Activity activity) {
            this.activity = activity;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String mesage) {
            this.mesage = mesage;
            return this;
        }

        public Builder setImgDialog(int imgDialog) {
            this.img_dialog = imgDialog;
            return this;
        }

        public Builder setContentButtonDismis(String content_button) {
            this.content_button_one = content_button;
            return this;
        }

        public Builder setContentButtonSubmit(String content_button) {
            this.content_button_two = content_button;
            return this;
        }

        public Builder isCancellable(boolean cancel) {
            this.cancel = cancel;
            return this;
        }

        public Builder setButtonDismisListener(UIDialogCustomListener btListener) {
            this.btListenerOne = btListener;
            return this;
        }

        public Builder setButtonSubmisListerner(UIDialogCustomListener btListener) {
            this.btListenerTwo = btListener;
            return this;
        }

        public Builder setTextColorTitle(int color) {
            this.colorTitle = color;
            return this;
        }

        public Builder setTextColorMessage(int color) {
            this.colorMessage = color;
            return this;
        }

        public Builder setBackgroudDrawableButtonDismis(Integer drawable) {
            this.backgroudDismis = drawable;
            return this;
        }

        public Builder setBackgroudDrawableButtonSubmis(Integer drawable) {
            this.backgroudSubmis = drawable;
            return this;
        }

        public Builder setOrientationButton(int orientationButton) {
            this.orientation = orientationButton;
            return this;
        }

        public Builder setMessageHtml(String htmlMessage) {
            this.messgaehtml = htmlMessage;
            return this;
        }
        public Builder setTextColorSubmis(int color) {
            this.color = color;
            return this;
        }
        public Builder setTextColorDismis(int color) {
            this.colorDismis = color;
            return this;
        }

        @SuppressLint("CheckResult")
        public UIDialogImageCustom build() {

            TextView txt_title, txt_message;
            ImageView imageView_dialog, btn_close_dialog;
            Button btn_dialog, btn_dialog_two;
            LinearLayout layout_button;

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_custom);
            txt_title = dialog.findViewById(R.id.txt_title_top_dialog);
            txt_message = dialog.findViewById(R.id.txt_content_dialog);
            imageView_dialog = dialog.findViewById(R.id.img_dialog);
            btn_close_dialog = dialog.findViewById(R.id.btn_cross);
            btn_dialog = dialog.findViewById(R.id.btn_dialog);
            btn_dialog_two = dialog.findViewById(R.id.btn_dialog_two);
            layout_button = dialog.findViewById(R.id.layout_button);
            if (color!=null){
                btn_dialog_two.setTextColor(color);
            }
            if (colorDismis!=null){
                btn_dialog.setTextColor(colorDismis);
            }
            if (orientation != null) {
                if (orientation == 1) {
                    layout_button.setOrientation(LinearLayout.VERTICAL);
                } else if (orientation == 0) {
                    layout_button.setOrientation(LinearLayout.HORIZONTAL);
                }
            }else {
                layout_button.setOrientation(LinearLayout.HORIZONTAL);
            }
            if (backgroudDismis != null) {
                btn_dialog.setBackgroundResource(backgroudDismis);
            }
            if (backgroudSubmis != null) {
                btn_dialog_two.setBackgroundResource(backgroudSubmis);
            }
            if (title != null) {
                txt_title.setVisibility(View.VISIBLE);
                txt_title.setText(title);
            } else {
                txt_title.setVisibility(View.GONE);
            }
            if (mesage != null&&messgaehtml==null) {
                txt_message.setVisibility(View.VISIBLE);
                txt_message.setText(mesage);
            } else if (mesage == null&&messgaehtml==null){
                txt_message.setVisibility(View.GONE);
            }
            if (messgaehtml!=null&&mesage==null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    txt_message.setText(Html.fromHtml(messgaehtml, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    txt_message.setText(Html.fromHtml(mesage));
                }
            } else if (mesage == null&&messgaehtml==null){
                txt_message.setVisibility(View.GONE);
            }







            if (content_button_one != null) {
                btn_dialog.setVisibility(View.VISIBLE);
                btn_dialog.setText(content_button_one);
                btn_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (btListenerOne != null) {
                            btListenerOne.onClick(dialog);

                        }
                    }
                });
            } else {
                btn_dialog.setVisibility(View.GONE);
            }
            if (content_button_two != null) {
                btn_dialog_two.setVisibility(View.VISIBLE);
                btn_dialog_two.setText(content_button_two);
                btn_dialog_two.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (btListenerTwo != null) {
                            btListenerTwo.onClick(dialog);

                        }
                    }
                });
            } else {
                btn_dialog_two.setVisibility(View.GONE);
            }
            if (content_button_two == null && content_button_one == null) {
                layout_button.setVisibility(View.GONE);
            } else {
                layout_button.setVisibility(View.VISIBLE);
            }

            if (img_dialog != null) {
                Glide.with(MyApplication.getAppContext()).load(img_dialog).into(imageView_dialog);
            } else {
                imageView_dialog.setVisibility(View.GONE);
            }

            btn_close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            if (colorTitle != null) {
                txt_title.setTextColor(colorTitle);
            }
            if (colorMessage != null) {
                txt_message.setTextColor(colorMessage);
            }

            dialog.show();
            return new UIDialogImageCustom();
        }
    }

    public interface UIDialogCustomListener {
        void onClick(Dialog dialog);
    }


}





