package com.water.drinkingwater.baseview;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;

import com.water.drinkingwater.HomeActivity;
import com.water.drinkingwater.R;

public class DialogProcessbar {
    public static class Builder {

        HomeActivity activity;
        int value = 0;
        sendValueProcess sendValueProcess;


        public Builder(HomeActivity homeActivity) {
            this.activity = homeActivity;
        }

        public Builder setProcess(int value) {
            this.value = value;
            return this;
        }

        public Builder setSenProcess(sendValueProcess senProcess) {
            this.sendValueProcess = senProcess;
            return this;
        }


        public DialogProcessbar build() {

            mTextview txt_total, txt_view;
            Button btn_cancel, btn_ok;
            SeekBar seekbar_id;
            Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_processbar);
            txt_total = dialog.findViewById(R.id.txt_total);
            txt_view = dialog.findViewById(R.id.txt_view);
            btn_cancel = dialog.findViewById(R.id.btn_cancel);
            btn_ok = dialog.findViewById(R.id.btn_ok);
            seekbar_id = dialog.findViewById(R.id.seekbar_id);
            seekbar_id.setMax(value);
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    sendValueProcess.senvalue(seekbar_id.getProgress(),0);
                }
            });
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    sendValueProcess.senvalue(seekbar_id.getProgress(),1);
                }
            });
            seekbar_id.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    txt_view.setText("+"+(progress*100/value)+"% ("+progress+" ml )");
                    txt_total.setText("Total: "+progress+" ml");

                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            dialog.show();
            return new DialogProcessbar();
        }

    }

    public interface sendValueProcess {
        void senvalue(int value,int keycallback);
    }
}
