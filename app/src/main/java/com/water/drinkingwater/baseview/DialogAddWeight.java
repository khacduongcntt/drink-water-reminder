package com.water.drinkingwater.baseview;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.water.drinkingwater.R;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.EntityWeight;

import java.util.ArrayList;

public class DialogAddWeight {
    public static class Builder implements BaseActivity.getdatePicker {
        UITextViewUnderlineCustom txt_date;
        private BaseActivity baseActivity;
        int day, month, yeah;
        DBManager dbManager;
        ArrayList<EntityWeight>arrayList = new ArrayList<>();
        UpdateChartWeight updateChartWeight;

        public Builder(BaseActivity baseActivity) {
            this.baseActivity = baseActivity;
            dbManager = new DBManager(baseActivity);
            baseActivity.creatModel(this);
            arrayList =dbManager.getWeightAll();
            yeah = baseActivity.getYear();
            month = baseActivity.getMonth();
            day = baseActivity.getDay();
        }
        public Builder setUpdateChartWeight(UpdateChartWeight updateChartWeight){
            this.updateChartWeight = updateChartWeight;
            return this;
        }


        public DialogAddWeight build() {

            ImageView close;
            EditText edt_weight;
            LinearLayout btn_edit_date;
            Button btn_ok, btn_cancel;

            Dialog dialog = new Dialog(baseActivity);
            dialog.setContentView(R.layout.dialog_add_weight);
            close = dialog.findViewById(R.id.btn_close);
            txt_date = dialog.findViewById(R.id.txt_date);
            edt_weight = dialog.findViewById(R.id.edt_weight);
            if (arrayList.size()>0) {
                edt_weight.setText(arrayList.get(arrayList.size() - 1).getWeight() + "");
            }else {
                edt_weight.setText("65");
            }
            btn_edit_date = dialog.findViewById(R.id.btn_edit_day);
            btn_ok = dialog.findViewById(R.id.btn_ok);
            btn_cancel = dialog.findViewById(R.id.btn_cancel);
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    float weight = Float.parseFloat(edt_weight.getText().toString());
                    dbManager.addWeight(new EntityWeight(0,yeah+"",day+"-"+month,weight));
                    updateChartWeight.updateChartWeight();
                    dialog.dismiss();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btn_edit_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    baseActivity.showDatePicker();
                }
            });

            dialog.show();


            return new DialogAddWeight();

        }


        @Override
        public void getdatepicker(String datepicker, int daypicker, int month, int yeah) {
            txt_date.setText(datepicker);
            this.day = daypicker;
            this.month = month;
            this.yeah = yeah;
        }
    }
    public interface UpdateChartWeight{
        void updateChartWeight();
    }
}
