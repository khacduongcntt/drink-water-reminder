package com.water.drinkingwater.baseview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.widget.TextView;



/**
 * creat by duongnk
 * UITextViewCustom creat 12/9/2019
 * -- dùng tromg xml:
 * dùng settext như bình thường dùng textview
 * -- dùng trong java:
 * myView.setContentTextview(MyString);
 */
@SuppressLint("AppCompatCustomView")
public class UITextViewUnderlineCustom extends TextView {
    public UITextViewUnderlineCustom(Context context) {
        super(context);
        init();

    }

    public UITextViewUnderlineCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UITextViewUnderlineCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setContentText(String contentString) {
        SpannableString content = new SpannableString(contentString);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        this.setText(content);
    }

    public void init() {
        SpannableString content = new SpannableString(this.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        this.setText(content);
        setTypeFaceRobotoNormal();
    }
    public void setTypeFaceRobotoNormal() {
        Typeface typeface ;
        typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");
        if (typeface != null)
            setTypeface(typeface);
    }
}
