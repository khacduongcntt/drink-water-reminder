package com.water.drinkingwater.util;

import android.content.Context;

import com.water.drinkingwater.sharedPreferences.TinyDB;

public class ControlSharePreferences {


    public static final String mGender = "gender";
    public static final String mWeigh = "weigh";
    public static final String mType = "typegender";

    Context context;
    TinyDB tinyDB;

    public ControlSharePreferences(Context context) {
        this.context = context;

        tinyDB = new TinyDB(context);

    }

    public void setSharePreferencesGender(int keyGender) {
        tinyDB.putInt(mGender, keyGender);
    }

    public void setSharePreferencesWeigh(int weigh, String type) {
        tinyDB.putString(mType, type);
        tinyDB.putInt(mWeigh, weigh);
    }


    public int getSharePreferencesGender() {
        return tinyDB.getInt(mGender, 0);
    }

    public int getWeigh() {

        return tinyDB.getInt(mWeigh, 55);
    }

    public String getTypeWeigh() {

        return tinyDB.getString(mType);
    }


}
