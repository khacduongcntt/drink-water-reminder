package com.water.drinkingwater.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.water.drinkingwater.R;
import com.water.drinkingwater.baseview.mTextview;
import com.water.drinkingwater.database.DBManager;
import com.water.drinkingwater.object.EntityCustomSizeWater;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Adapter_sizeWater extends RecyclerView.Adapter<Adapter_sizeWater.ViewHolder> {

    ArrayList<EntityCustomSizeWater> arrayList;
    Context context;
    String path;
    longClickItemWatet longClickItemWatet;
    DBManager dbManager;
    OnclickWater onclickWater;
    boolean ischeck = false;



    public Adapter_sizeWater(ArrayList<EntityCustomSizeWater> arrayList, Context context, String path, longClickItemWatet longClickItemWatet,OnclickWater onclickWater) {
        this.arrayList = arrayList;
        this.context = context;
        this.path = path;
        this.longClickItemWatet = longClickItemWatet;
        this.onclickWater = onclickWater;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_water, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        dbManager = new DBManager(context);
        if (ischeck){
            holder.btn_delete_cup.setVisibility(View.VISIBLE);
        }else {
            holder.btn_delete_cup.setVisibility(View.INVISIBLE);
        }
        Glide.with(context).load(path + arrayList.get(position).getPath_icon()).into(holder.img_incon);
        holder.item_water.setText(arrayList.get(position).getSize_water());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ischeck = true;
                notifyDataSetChanged();
                return false;
            }

        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ischeck = false;
                notifyDataSetChanged();
                onclickWater.onclickwater(position,path + arrayList.get(position).getPath_icon(),arrayList.get(position).getSize_water());
            }
        });
        holder.btn_delete_cup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbManager.deleteSizeWater(arrayList.get(position).getId());
                longClickItemWatet.longClickItem(position);
                ischeck = false;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_icon)
        ImageView img_incon;
        @BindView(R.id.item_water)
        mTextview item_water;
        @BindView(R.id.btn_delete_cup)
        ImageView btn_delete_cup;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface longClickItemWatet {
        void longClickItem(int possion);
    }
    public interface OnclickWater{
        void onclickwater(int possison,String path_icon,String totalwater);
    }
}
