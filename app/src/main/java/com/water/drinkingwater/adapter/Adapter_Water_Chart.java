package com.water.drinkingwater.adapter;

import android.content.Context;
import android.graphics.Color;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.water.drinkingwater.R;
import com.water.drinkingwater.baseview.BaseActivity;
import com.water.drinkingwater.baseview.mTextview;
import com.water.drinkingwater.object.Entity_Water_Day;
import com.water.drinkingwater.object.Entity_water;
import com.ramijemli.percentagechartview.PercentageChartView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ramijemli.percentagechartview.renderer.BaseModeRenderer.MODE_PIE;

public class Adapter_Water_Chart extends RecyclerView.Adapter<Adapter_Water_Chart.ViewHolder> {


    private ArrayList<Entity_water> arrayList_water = new ArrayList<>();
    private ArrayList<Entity_Water_Day> arrayList_water_day = new ArrayList<>();
    BaseActivity baseActivity;
    Context context;


    public Adapter_Water_Chart(ArrayList<Entity_water> arrayList_water, ArrayList<Entity_Water_Day> arrayList_water_day, BaseActivity baseActivity, Context context) {
        this.arrayList_water = arrayList_water;
        this.arrayList_water_day = arrayList_water_day;
        this.baseActivity = baseActivity;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chart_water_v4, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        if (arrayList_water_day.size() > 0 && arrayList_water.size() > 0) {
            try {
                for (int i = 0; i <arrayList_water_day.size(); i++) {
                    float water = (baseActivity.getWaterday(arrayList_water_day.get(position).getDate()) * 100) / arrayList_water_day.get(position).getWater();
                    if (water > 100) {
                        water = 100;
                    }
                    holder.mRingChart.setProgress(water, true);
                    holder.txt_date.setText(arrayList_water_day.get(position).getDate());
                    holder.txt_total.setText(baseActivity.getWaterday(arrayList_water_day.get(position).getDate()) + "ml / " + arrayList_water_day.get(position).getWater() + "ml");
                }


            } catch (Exception e) {
                holder.mRingChart.setProgress(0, true);
                holder.txt_date.setText("");
                holder.txt_total.setText(0 + "ml / " + 0 + "ml");
            }
        } else {
            holder.mRingChart.setProgress(0, true);
            holder.txt_date.setText("Date");
            holder.txt_total.setText(0 + "ml / " + 0 + "ml");
        }

    }

    @Override
    public int getItemCount() {
        return arrayList_water_day.size();
    }

    private int displayedMode;
    private Transition fade;
    private int colorOne;
    private int colorTwo;
    private int colorThree;
    private int colorFour;
    private int gradientType;
    private int gradientAngle;


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ring_chart)
        PercentageChartView mRingChart;
        @BindView(R.id.txt_date)
        mTextview txt_date;
        @BindView(R.id.txt_total_of_day)
        mTextview txt_total;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            colorOne = Color.parseColor("#7AF5F5");
            colorTwo = Color.parseColor("#FFEA00");
            colorThree = Color.parseColor("#1FC7F1");
            colorFour = Color.parseColor("#079EC4");

            gradientType = 2;
            gradientAngle = 90;


            setupLayoutAnimation();

            mRingChart.setGradientColors(gradientType, new int[]{colorOne, colorOne, colorThree, colorFour}, null, gradientAngle);
        }
    }

    private void setupLayoutAnimation() {
        Explode transition = new Explode();
        transition.setDuration(600);
        transition.setInterpolator(new OvershootInterpolator(1f));
        baseActivity.getWindow().setEnterTransition(transition);

        displayedMode = MODE_PIE;
        fade = new Fade();
        fade.setDuration(1000);
    }

}
