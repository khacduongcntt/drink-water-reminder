package com.water.drinkingwater.adapter;

import android.content.Context;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.water.drinkingwater.R;
import com.water.drinkingwater.object.Entity_select_itemview;

import java.util.ArrayList;

public class Adapter_sticker extends RecyclerView.Adapter<Adapter_sticker.ViewHolder> {

    ArrayList arr_icon;
    Context mContext;
    Onclickpossisonsticker onclickpossisonsticker;
    String pathAcces;
    ArrayList<Entity_select_itemview> arrayList_select = new ArrayList<>();
    int clickBefore =0;

    public Adapter_sticker(ArrayList arr_icon2, Context mContext, Onclickpossisonsticker onclickpossisonsticker1,String path) {
        arr_icon = arr_icon2;
        this.mContext = mContext;
        this.onclickpossisonsticker = onclickpossisonsticker1;
        this.pathAcces = path;
        for (int i = 0; i <arr_icon2.size() ; i++) {
            arrayList_select.add(new Entity_select_itemview(arr_icon.get(i).toString(),false));
        }
    }

    @NonNull
    @Override
    public Adapter_sticker.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_icon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_sticker.ViewHolder holder, final int position) {
        if (arrayList_select.get(position).getischeckSelect()){
            holder.img.setBackgroundColor(Color.GRAY);
        }else {
            holder.img.setBackgroundColor(Color.WHITE);
        }


        Glide.with(mContext).load(pathAcces + arr_icon.get(position)).into(holder.img);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclickpossisonsticker.Onclicksticker(position);
                if (!arrayList_select.get(position).getischeckSelect()){
                    arrayList_select.get(clickBefore).setischeckSelect(false);
                    arrayList_select.get(position).setischeckSelect(true);
                    clickBefore = position;
                }else {
                    arrayList_select.get(position).setischeckSelect(false);
                }
                notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return arr_icon.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_icon_main);
        }
    }

    public interface Onclickpossisonsticker {
        void Onclicksticker(int posison);
    }
}
