package com.water.drinkingwater.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.water.drinkingwater.R;
import com.water.drinkingwater.baseview.mTextview;
import com.water.drinkingwater.object.Entity_water;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Adapter_Water extends RecyclerView.Adapter<Adapter_Water.ViewHolder> {


    ArrayList<Entity_water> arrayList;
    Context mContext;
    OnclickWaterSale onclickWater;

    public Adapter_Water(ArrayList<Entity_water> arrayList, Context mContext,OnclickWaterSale onclickWater1) {
        this.arrayList = arrayList;
        this.mContext = mContext;
        this.onclickWater = onclickWater1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sale_water, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext).load(arrayList.get(position).getImg_water()).into(holder.img_icon);
        holder.txt_time.setText(arrayList.get(position).getTime_water());
        holder.txt_water.setText(arrayList.get(position).getTotal_water());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclickWater.onclclickWaterSale(arrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_icon)
        ImageView img_icon;
        @BindView(R.id.txt_water)
        mTextview txt_water;
        @BindView(R.id.txt_time)
        mTextview txt_time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface OnclickWaterSale{
        void onclclickWaterSale(Entity_water entity_water);
    }
}
