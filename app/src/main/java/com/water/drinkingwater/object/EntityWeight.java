package com.water.drinkingwater.object;

public class EntityWeight {

    int id;
    String yeah;
    String date_month;
    float weight;

    public EntityWeight() {
    }

    public EntityWeight(int id, String yeah, String date_month, float weight) {
        this.id = id;
        this.yeah = yeah;
        this.date_month = date_month;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYeah() {
        return yeah;
    }

    public void setYeah(String yeah) {
        this.yeah = yeah;
    }

    public String getDate_month() {
        return date_month;
    }

    public void setDate_month(String date_month) {
        this.date_month = date_month;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
