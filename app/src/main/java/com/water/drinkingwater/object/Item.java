package com.water.drinkingwater.object;

import java.util.ArrayList;
import java.util.List;

import top.defaults.view.PickerView;

public class Item implements PickerView.PickerItem {
    public int text;
    public String type;

    public Item(int s) {
        text = s;
    }

    public Item(String type) {
        this.type = type;
    }

    @Override
    public String getText() {
        return text+"";
    }

    public void setText(int text) {
        this.text = text;
    }
    public int getKg(){
        return text;
    }
    public String gettype(){
        return type;
    }

    public static List<Item> sampleItems() {
        List<Item> items = new ArrayList<>();
        for (int i = 5; i < 365; i++) {
            items.add(new Item( i));
        }
        return items;
    }

}