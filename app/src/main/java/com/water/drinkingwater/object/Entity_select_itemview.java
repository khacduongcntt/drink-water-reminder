package com.water.drinkingwater.object;

public class Entity_select_itemview {
    String value;
    Boolean ischeckSelect;

    public Entity_select_itemview(String value, Boolean ischeckSelect) {
        this.value = value;
        this.ischeckSelect = ischeckSelect;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getischeckSelect() {
        return ischeckSelect;
    }

    public void setischeckSelect(Boolean aBoolean) {
        this.ischeckSelect = aBoolean;
    }
}
