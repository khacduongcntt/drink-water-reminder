package com.water.drinkingwater.object;

import java.util.ArrayList;
import java.util.List;

import top.defaults.view.PickerView;

public class Item_kg implements PickerView.PickerItem {
    public static final String kg = "kg";
    public static final String lb = "lb";

    public String type;



    public Item_kg(String type) {
        this.type = type;
    }

    @Override
    public String getText() {
        return type;
    }


    public String gettype(){
        return type;
    }

    public static List<Item_kg> sampleItemsTylpe() {
        List<Item_kg> item_kg = new ArrayList<>();
        item_kg.add(new Item_kg(lb));
        item_kg.add(new Item_kg(kg));

        return item_kg;
    }
}