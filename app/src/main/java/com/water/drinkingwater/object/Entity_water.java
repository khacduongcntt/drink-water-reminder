package com.water.drinkingwater.object;

public class Entity_water {
    int id;
    String time_water;
    String date_water;
    String img_water;
    String total_water;

    public Entity_water(int id, String time_water, String date_water, String img_water, String total_water) {
        this.id = id;
        this.time_water = time_water;
        this.date_water = date_water;
        this.img_water = img_water;
        this.total_water = total_water;
    }

    public Entity_water() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime_water() {
        return time_water;
    }

    public void setTime_water(String time_water) {
        this.time_water = time_water;
    }

    public String getDate_water() {
        return date_water;
    }

    public void setDate_water(String date_water) {
        this.date_water = date_water;
    }

    public String getImg_water() {
        return img_water;
    }

    public void setImg_water(String img_water) {
        this.img_water = img_water;
    }

    public String getTotal_water() {
        return total_water;
    }

    public void setTotal_water(String total_water) {
        this.total_water = total_water;
    }
}
