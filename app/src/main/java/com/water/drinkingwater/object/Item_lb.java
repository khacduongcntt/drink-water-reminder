package com.water.drinkingwater.object;

import java.util.ArrayList;
import java.util.List;

import top.defaults.view.PickerView;

public class Item_lb implements PickerView.PickerItem {
    public int text;
    public String type;

    public Item_lb(int s) {
        text = s;
    }

    public Item_lb(String type) {
        this.type = type;
    }

    @Override
    public String getText() {
        return text+"";
    }

    public void setText(int text) {
        this.text = text;
    }
    public int getKg(){
        return text;
    }
    public String gettype(){
        return type;
    }

    public static List<Item_lb> sampleItems() {
        List<Item_lb> items = new ArrayList<>();
        for (int i = 5; i < 365; i++) {
            items.add(new Item_lb((int) (i*2.2)));
        }
        return items;
    }

}