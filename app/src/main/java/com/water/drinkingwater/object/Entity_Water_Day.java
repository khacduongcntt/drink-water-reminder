package com.water.drinkingwater.object;

public class Entity_Water_Day {
    int id;
    String date;
    int water;
    String value;

    public Entity_Water_Day() {
    }

    public Entity_Water_Day(int id, String date, int water, String value) {
        this.id = id;
        this.date = date;
        this.water = water;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
