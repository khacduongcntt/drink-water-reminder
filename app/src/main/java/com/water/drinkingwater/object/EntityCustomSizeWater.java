package com.water.drinkingwater.object;

public class EntityCustomSizeWater {
    int id;
    String path_icon;
    String size_water;

    public EntityCustomSizeWater() {
    }

    public EntityCustomSizeWater( String path_icon, String size_water) {
        this.path_icon = path_icon;
        this.size_water = size_water;
    }

    public String getPath_icon() {
        return path_icon;
    }

    public void setPath_icon(String path_icon) {
        this.path_icon = path_icon;
    }

    public String getSize_water() {
        return size_water;
    }

    public void setSize_water(String size_water) {
        this.size_water = size_water;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
