package com.water.drinkingwater;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class FileAcces {

    public ArrayList<String> getImage(Context context, String nameFolder) {
        ArrayList<String> listStrImage = new ArrayList<>();
        AssetManager assetManager = context.getAssets();
        String[] files = new String[0];
        try {
            files = assetManager.list(nameFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < Arrays.asList(files).size(); i++) {
            listStrImage.add(Arrays.asList(files).get(i));
        }
        return listStrImage;
    }

}

